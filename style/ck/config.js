/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.toolbar= [
      { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Preview', '-', 'Templates' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
       { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline','list', 'indent', 'blocks',] },
       { name: 'paragraph', groups: [  'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Blockquote',  '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-',    ] },
       { name: 'links', items: [ 'Link', 'Unlink' ] },
       { name: 'insert', items: [ 'Image','Youtube', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
       { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] }, 
        { name: 'others', items: [ '-' ] },
       
    ];
     config.extraPlugins = 'Youtube';
        config.youtube_width = '640';
        config.youtube_height = '480';
        config.youtube_responsive = true;
        config.youtube_older = false;
        config.youtube_related = true;
        config.youtube_autoplay = false;
        config.youtube_controls = true;
        config.youtube_privacy = false;
};
