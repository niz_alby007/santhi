var App = (function ($) {
    "use strict";

    var defaults = {
        'site_url': '',
        'base_url': '',
        'site_name': '',
        'admin_folder': '',
        'user_role':'',
    }
    var config = {};

    var appInit = function ( options ) {
        config = $.extend({}, defaults, options);
    }
    var siteUrl = function ( uri ) {
        uri = uri || '';
        return config.site_url + uri;
    }

        return {
        init: function (options) {
            appInit(options);
         //   handleUI();
        },
        siteUrl: function ( uri ) {
            return siteUrl(uri);
        },
        adminUrl: function(uri){
            return adminUrl(uri);
        },
        agentUrl: function(uri){
            return agentUrl(uri);
        },
        baseUrl: function ( uri ) {
            return baseUrl(uri);
        },
        siteName: function () {
            return siteName();
        },
        userRole: function(){
            return userRole();
        },
        growl: function ( msg ) {
            handleGrowl(msg);
        },
        confirm: function ( title, body, callback ) {
            showConfirmModal(title, body, callback);
        },
        alert: function (message, title) {
            showAlertModal(message, title);
        },
        makeSafeName: function ( val, replacement ) {
            return makeSafeName(val, replacement);
        },
        initTreeView: function() {
            handleTreeView();
        }
    }

})(jQuery);