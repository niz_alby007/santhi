
	facilities();
	function facilities() {
		$('#facilities').DataTable( { 
			pageLength:20,
           	'processing': true,
            'serverSide': true,
            stateSave: true,
            "bDestroy": true,
            'responsive':true,
			"order": [[ 0, "ASC" ]],
            'serverMethod': 'post',
               "ajax": {
                    "url":App.siteUrl()+'admin/facilities/list',
                    "type": "POST",
                },
                "columns": [
	                {"data":"id"},
	                {"data": "title"},
					{"data": "short_description"},
                    {"data": "path"},
	                {"data":"status"} 
                ],
                'columnDefs': [ {
                              
                }],
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                    return nRow;
                }
         } );
	}
	

function deletefacilities(e){
	if(confirm('are you sure. You want to delete ')) {
		var id     = $(e).attr('data-id');
		var tbl    = $(e).attr('data-tbl');
		var tblid  = $(e).attr('data-tblid')
		var folder = $(e).attr('data-folder');
		var path   = $(e).attr('data-path');
		if (id !='') {
			$.ajax({
				type   : 'post',
				url    : App.siteUrl()+'admin/facilities/delete',
				data   : {'id':id,'tbl':tbl,'tblid':tblid,path:path,'folder':folder},
				dataType : 'json',
				success:function(res) {
					if(res.status == 200 ){
						swal('Dleted',res.msg,'success');
						facilities();
					}
				}

			})
		}
	}
}