$proType = $("#property_type").select2({
    placeholder: 'Select Property Type',
   allowClear: false,
   //minimumResultsForSearch: -1
 });

 $category = $("#category").select2({
    placeholder: 'Select Property Type',
    allowClear: false,
    minimumResultsForSearch: -1
 });

 $developer = $("#developer").select2({
    placeholder: 'Select Developer ',
   allowClear: false,
   minimumResultsForSearch: -1
 });

 $country_by_states = $("#country_by_states").select2({
    placeholder: 'Select Developer ',
   allowClear: false,
   minimumResultsForSearch: -1
 });

 $state_by_district = $("#state_by_district").select2({
    placeholder: 'Select Developer ',
   allowClear: false,
   minimumResultsForSearch: -1
 });

 $state_by_city = $("#state_by_city").select2({
    placeholder: 'Select Developer ',
   allowClear: false,
   minimumResultsForSearch: -1
 });

 
 $state_by_city.data('select2').$container.addClass('form-select');
 $state_by_district.data('select2').$container.addClass('form-select');
 $country_by_states.data('select2').$container.addClass('form-select');
 $developer.data('select2').$container.addClass('form-select');
 $proType.data('select2').$container.addClass('form-select');
 $category.data('select2').$container.addClass('form-select');


//  CKEDITOR.replace( 'description',{
// 		toolbar:[]
// 	} );

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
    };
      reader.readAsDataURL(input.files[0]);
    }
  }

    
$("#property_type").change(function() {
    if ($(this).val() == "villa" || $(this).val() == "house" || $(this).val() == "flat" || $(this).val() == "appartments" ) {
        $('.cateAppartment').show();
            var property_id = $('#de_cryptor').val();
          //  alert($(this).val());
        $.ajax({
         url:App.siteUrl()+'admin/properties/flatAppartmentHouseVilla',
         method:'POST',
         data:{id:property_id},
            success:function(res){
                //$('#guestHouseAppartment_sec').show();
                $('#propertyManageSec').html(res);
            }
        })
                                        
        //$('#facingwarap').html('<div class="form-group"><label class="col-form-label" for="inputSuccess"> Facing </label> <select name="facing" id="facing" class="form-control select2"><option value="east">East</option><option value="west">West</option><option value="North">North</option><option value="South">South</option> </select></div>');
        $('#otherField1').attr('required','');
        $('#otherField1').attr('data-error', 'This field is required.');
        $('#otherField2').attr('required','');
        $('#otherField2').attr('data-error', 'This field is required.');
    } else {
        $('.cateAppartment').hide();
        $('#otherField1').removeAttr('required');
        $('#otherField1').removeAttr('data-error');
        $('#otherField2').removeAttr('required');
        $('#otherField2').removeAttr('data-error');
        $('#propertyManageSec').html(' ');
    }
});


$("#property_type").change(function() {
    var property_id = $('#de_cryptor').val();
    
            if ($(this).val() == "land" ) {
                $('#ploatDtl').show();
                $.ajax({
                    url:App.siteUrl()+'admin/properties/land',
                    method:'POST',
                    data:{id:property_id},
                    success:function(res){
                        //$('#guestHouseAppartment_sec').show();
                        $('#propertyManageSec').html(res);
                    }
                })    				

                $('#otherField1').attr('required','');
                $('#otherField1').attr('data-error', 'This field is required.');
                $('#otherField2').attr('required','');
                $('#otherField2').attr('data-error', 'This field is required.');
                $('#propertyManageSec').html(' ');
            } else {
                $('#ploatDtl').hide();
                $('#otherField1').removeAttr('required');
                $('#otherField1').removeAttr('data-error');
                $('#otherField2').removeAttr('required');
                $('#otherField2').removeAttr('data-error');
                $('#propertyManageSec').html('');
            }
        });

        $("#property_type").change(function() {
            if ( $(this).val() == "office" || $(this).val() == "shop" ) {
                 var property_id = $('#de_cryptor').val();

                $('.office-shop').show();
                
                 $.ajax({
                    url:App.siteUrl()+'admin/properties/shopOffice',
                    method:'POST',
                    data:{id:property_id},
                    success:function(res){
                        //$('#guestHouseAppartment_sec').show();
                        $('#propertyManageSec').html(res);
                    }
                })   
        
                $('#otherField1').attr('required','');
                $('#otherField1').attr('data-error', 'This field is required.');
               $('#otherField2').attr('required','');
                $('#otherField2').attr('data-error', 'This field is required.');
            } else {
                $('#propertyManageSec').html(' ');
                $('.office-shop').hide();
                $('#otherField1').removeAttr('required');
                $('#otherField1').removeAttr('data-error');
                $('#otherField2').removeAttr('required');
                $('#otherField2').removeAttr('data-error');
            }
        });
        $("#property_type").change(function() {
            if ($(this).val() == "warehouse" ) {
                var property_id = $('#de_cryptor').val();

                $('.godown-warehouse').show();
                
            $.ajax({
                url:App.siteUrl()+'admin/properties/warehouse',
                 method:'POST',
                 data:{id:property_id},
                    success:function(res){
                        //$('#guestHouseAppartment_sec').show();
                        $('#propertyManageSec').html(res);
                    }
                })

                $('#category').attr("disabled", 'disabled');
                $('.mainCategory').val('2').trigger("change");
                $('#otherField1').attr('required','');
                $('#otherField1').attr('data-error', 'This field is required.');
                $('#otherField2').attr('required','');
                $('#otherField2').attr('data-error', 'This field is required.');

                 $(".mainCategory option[value='1']").remove();
                 $('.mainCategory').val('2').trigger("change");

            } else {
                $('.mainCategory').prop("disabled", false);
                $('.mainCategory').trigger("change");
                $('.godown-warehouse').hide();

                $('#otherField1').removeAttr('required');
                $('#otherField1').removeAttr('data-error');
                $('#otherField2').removeAttr('required');
                $('#otherField2').removeAttr('data-error');
                $('#propertyManageSec').html( ' ');
            }
        });

        $(".property_type_ct").change(function() {
            if ($(this).val() == "pg" ||  $(this).val() == "guesthouse") {
                 var property_id = $('#de_cryptor').val();
            
                if( $(this).val() ==="guesthouse")
                {
                    $.ajax({
                        url:App.siteUrl()+'admin/properties/guestHouseAppartment',
                        method:'POST',
                        data:{id:property_id},
                        success:function(res)
                        {
                            //$('#guestHouseAppartment_sec').show();
                            $('#propertyManageSec').html(res);
                        }
                    })
                    }else{
                        $('#propertyManageSec').empty();
                          //$('#guestHouseAppartment_sec').hide();
                           $('.mainCategory').trigger("change");
                    }
                     if( $(this).val() ==="pg"){
                         $('.pg-hostel').show();
                         $.ajax({
                         url:App.siteUrl()+'admin/properties/pg',
                         method:'POST',
                         data:{id:property_id},
                            success:function(res){
                                //$('#guestHouseAppartment_sec').show();
                                $('#propertyManageSec').html(res);
                            }
                        })
                     }else{
                         $('#propertyManageSec').html(' ');
                         $('.pg-hostel').hide();
                         $('.mainCategory').trigger("change");
                     }
                
                //$('.mainCategory').prop("disabled", true);
                 $(".mainCategory option[value='1']").remove();
                $('.mainCategory').val('2').trigger("change");

                $('#otherField1').attr('required','');
                $('#otherField1').attr('data-error', 'This field is required.');
                $('#otherField2').attr('required','');
                $('#otherField2').attr('data-error', 'This field is required.');
            } else {
                
                $('.guestHouseAppartment').hide();
                 $('#propertyManageSec').empty();
                $('.pg-hostel').hide();
                $('#otherField1').removeAttr('required');
                $('#otherField1').removeAttr('data-error');
                $('#otherField2').removeAttr('required');
                $('#otherField2').removeAttr('data-error');
                $('.mainCategory').prop("disabled", false);

            }
        });

    



 $(function() {

    $("#propertyForm").validate({
      rules: {
  
        property_type: {
          required: true,
        },
        category:{
            required: true,
        },
        amount:{
            required:true,
            digits: true
        },
        property_name:{
            required:true,
        },
        description: {
          required: true,
        },
        
        
      },    messages: {
        category: {
          required: "Please enter some data"
        },
      },
      submitHandler: function(form,e) {
              e.preventDefault();
              for (instance in CKEDITOR.instances) {
                  CKEDITOR.instances[instance].updateElement();
              }
  
              $('#propertyForm .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
              $('#propertyForm .btn-primary' ).prop('disabled', true);
              var formData =  new FormData(document.getElementById('propertyForm'));
              
              $.ajax({
                  method : form.method,
                  url    : form.action,
                  data   : formData,
                  contentType: false,
                  processData: false,
                  dataType : 'json',
                  
                  success:function(res) {
                      if(res.status == 200) {
                          swal("Good job!", res.msg, "success");
                          setTimeout(function() {
                              $('#newsEditor .btn-primary' ).html('Submit');
                              $('#newsEditor .btn-primary' ).prop('disabled', false);
                              $('#newsEditor').trigger("reset");
                          }, 200);
                    
                    //window.location.replace('https://api.whatsapp.com/send?text=*' + res.title + '%20' +'%20'+ res.short_description+'%20'+ res.slug+ '%20' + '<?=$this->config->item("join");?>' + '' );
  
                      }else{
                    $('#newsEditor .btn-primary' ).html('submit');
                    $('#newsEditor .btn-primary' ).prop('disabled', false);
                          $('#newsEditor .btn-primary' ).html('Submit');
                          swal('oops',res.msg,'error');
                      }
                  }
  
              })
              return true;
          }
    });
  });
  

  
function deleteProImg(e){
	if(confirm('are you sure. You want to delete ')) {
		var id     = $(e).attr('data-id');
		var tbl    = $(e).attr('data-tbl');
		var tblid  = $(e).attr('data-tblid')
		var folder = $(e).attr('data-folder');
		var path   = $(e).attr('data-path');
		if (id !='') {
			$.ajax({
				type   : 'post',
				url    : App.siteUrl()+'admin/delete/delete',
				data   : {'id':id,'tbl':tbl,'tblid':tblid,path:path,'folder':folder},
				dataType : 'json',
				success:function(res) {
					if(res.status == 200 ){
						swal('Dleted',res.msg,'success');
					}
				}

			})
		}
	}
}