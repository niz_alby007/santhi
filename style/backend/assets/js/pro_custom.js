var limit = 9;
var start = 0;
var action = 'inactive';
var start = 0;
var page = 0;

 function getCitybyproperty(){
      setTimeout(function(){ load_prd_category_data(9,0); $('#property_details').html(""); }, 1000);
   }

   
 function load_prd_category_data(limit, start)
 {
  var superBuidtupareaMin = $('.input-min').val();
  var superBuidtupareamax = $('.input-max').val();

  var minCarParking = $('.parking-min').val();
  var maxCarParking = $('.parking-max').val();

  var landminPrice = $('.land-input .landinput-min').val();
  var landmaxPrice = $('.land-input .landinput-max').val();

  var landminArea = $('.ploat-input .landinput-min').val();
  var landmaxArea = $('.ploat-input .landinput-max').val();

  var bedrooms = new Array();
        $(".bedrromfilter input:checked").each(function() {
           bedrooms.push($(this).val());
        });

  var bathroom = new Array();
        $(".bathroomfilter input:checked").each(function() {
           bathroom.push($(this).val());
        });
        
  var furnishedStatus     = $( '.propertyfurnishedfilterwrap input[name=furnished_ststus]:checked' ).val();
  var bachelors_status    = $( '.bachelorsfilterwrap input[name=bachelors_status]:checked' ).val();
  var construction_status = $( '.construction_status input[name=construction_status]:checked' ).val();
  var roomsharing         = $( '.roomsharing input[name=room_sharing]:checked' ).val();
  var measlStatus         = $( '.measlStatus input[name=meals_status]:checked' ).val();
  var searchKey           = $( '.searchkey' ).val();


  $.ajax({
   url: App.siteUrl()+'admin/properties/load_list',//"<?=base_url('properties/load_list')?>",
   method:"POST",
   async:true,
   dataType: "json",
   data:{category_id:'','search':$("#search").val(),
   'property_type':$("#sector").val(),
   'category':$("#category").val(),
   minprice:$("#min_price").val(),
   maxprice:$("#max_price").val(),locations:$('#cityDiv').val(),
  bedrooms: bedrooms,
  bathroom:bathroom,
  furnishedStatus : furnishedStatus,
  bachelors_status : bachelors_status,
  construction_status : construction_status,
  superBuidtupareaMin : superBuidtupareaMin,
  superBuidtupareamax  : superBuidtupareamax,
  minCarParking : minCarParking,
  maxCarParking : maxCarParking,
  roomsharing   : roomsharing,
  landminPrice  : landminPrice,
  landmaxPrice  : landmaxPrice,
  landminArea   : landminArea,
  landmaxArea   : landmaxArea,
  measlStatus   : measlStatus,
  searchKey     : searchKey,
   'limit':limit, 'start':start},
   cache:false,
   success:function(data)
   {
       //console.log(data.qry);
   
        if(data.status == 200){
         $('#productFilterBtn').html('<i class="fa fa-filter"></i>Search');
         $('#productFilterBtn').attr('disabled',false) 
          $('#property_details').append(data.response).fadeIn(); ;
        }
    if(data.status == '')
    {
       $('#productFilterBtn').html('<i class="fa fa-filter"></i>Search');
         $('#productFilterBtn').attr('disabled',false) 

     $('#load-msg').html("<div class='col-sm-12'><div class='clearfix'></div><div class='alert alert-info'><strong>Sorry</strong><p>No data to list</p></div></div>");
     action = 'active';
    }
    else if(data.status==202){
       $('#productFilterBtn').html('<i class="fa fa-filter"></i>Search');
         $('#productFilterBtn').attr('disabled',false) 
         
      $('#load-msg').html(" ");  
        action = 'active';

    }
    else
    {
     $('#load-msg').html("<div class='col-sm-12'><button type='button' class='btn btn-warning btn_pls_wait'>Please Wait....</button></div>");
     action = "inactive";

    }
    
    // else if(data.status==404)
    // {
    //   $('#property_details_message').html("<div class='col-sm-12'></div>"); 
    //   action = "active";
    // }
    
     $('body').removeClass("my-loading");
   },error:function(data){
   
   }
  });
 }



 if(action == 'inactive')
 {
  action = 'active';
  load_prd_category_data(limit, start);
 }





 $(window).scroll(function(){
  if($(window).scrollTop() + $(window).height() > $("#property_details").height() && action == 'inactive')
  {
   action = 'active';
   start = start + limit;
  
   
   setTimeout(function(){
    load_prd_category_data(limit, start);
   }, 1000);

   
  }
 });
 

 function rentout(e){
   if (confirm("Are you sure?")) {
          $.ajax({
     method :"POST",
     url    : App.siteUrl()+"admin/rentout_properties/rentout",
     data   : {id:e},
     dataType : 'json',
     success:function(res){
       if(res.status == 200){
         swal('Done..',res.messages,'success');
         
          //dataloader();
          $('#property'+e).addClass('d-none');
       }else{
         swal('Opps..',res.msg,'error');
         
       }
     }
     })
       return false;
   }
 }
 
 
 function soldOut(e){
   if (confirm("Are you sure?")) {
          $.ajax({
     method :"POST",
     url    :App.siteUrl()+"admin/rentout_properties/soldOut",
     data   : {id:e},
     dataType : 'json',
     success:function(res){
       if(res.status == 200){
         swal('Done..',res.messages,'success');
          dataloader();
       }else{
         swal('Opps..',res.msg,'error');
       }
 
     }
     })
       return false;
   }
 }
 
 