departments();
function departments() {
	$('#departments').DataTable( { 
           	'processing': true,
            'serverSide': true,
            stateSave: true,
            "bDestroy": true,
            'responsive':true,
            "order": [[ 0, "desc" ]],
            'serverMethod': 'post',
               "ajax": {
                    "url":App.siteUrl()+'admin/departments/list',
                    "type": "POST",
                },
                "columns": [
                 	{"data":"id"},
	                {"data":"department_id"},
	                {"data": "doctors"},
	                {"data": "path"},
	                {"data":"status"}
                ],
                'columnDefs': [ {
                              
                }],
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                    return nRow;
            }
    } );
}

function deleteDepartment(e){
	if(confirm('are you sure. You want to delete ')) {
		var id     = $(e).attr('data-id');

		if (id !='') {
			$.ajax({
				type   : 'post',
				url    : App.siteUrl()+'admin/departments/delete',
				data   : {'id':id},
				dataType : 'json',
				success:function(res) {
					if(res.status == 200 ){
						swal('Dleted',res.msg,'success');
						departments();
					}
				}

			})
		}
	}
}


