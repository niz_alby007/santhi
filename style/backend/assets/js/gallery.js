
	servicesTbl();
	function servicesTbl() {
		$('#servicesTbl').DataTable( { 
           	'processing': true,
            'serverSide': true,
            stateSave: true,
            "bDestroy": true,
            'responsive':true,
			"order": [[ 0, "desc" ]],
            'serverMethod': 'post',
               "ajax": {
                    "url":App.siteUrl()+'admin/gallery/list',
                    "type": "POST",
                },
                "columns": [
	                {"data":"id"},
	                {"data": "title"},
					{"data": "path"},
	                {"data":"status"} 
                ],
                'columnDefs': [ {
                              
                }],
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                    return nRow;
                }
         } );
	}
	

function deleteGallery(e){
    if(confirm('are you sure. You want to delete ')) {
        var id     = $(e).attr('data-id');
        var tbl    = $(e).attr('data-tbl');
        var tblid  = $(e).attr('data-tblid')
        var folder = $(e).attr('data-folder');
        var path   = $(e).attr('data-path');

        if (id !='') {
            $.ajax({
                type   : 'post',
                url    : App.siteUrl()+'admin/delete/delete',
                data   : {'id':id,'tbl':tbl,'tblid':tblid,path:path,folder:folder},
                dataType : 'json',
                success:function(res) {
                    if(res.status == 200 ){
                        swal('Dleted',res.msg,'success');
                        servicesTbl();
                    }
                }

            })
        }
    }
}