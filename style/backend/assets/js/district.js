
district();
function district() {
    $('#stateTbl').DataTable( { 
           'processing': true,
        'serverSide': true,
        stateSave: true,
        "bDestroy": true,
        'responsive':true,
        "order": [[ 0, "desc" ]],
        'serverMethod': 'post',
           "ajax": {
                "url":App.siteUrl()+'admin/locations/district_list',
                "type": "POST",
            },
            "columns": [
                {"data":"district_id"},
                {"data":"country"},
                {"data": "state_name"},
                {"data": "district_name"},
                {"data":"action"}
            ],
            'columnDefs': [ {
                          
            }],
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
            }
     } );
}


function deleteDistrict(e){
if(confirm('are you sure. You want to delete ')) {
    var id     = $(e).attr('data-id');
    var tbl    = $(e).attr('data-tbl');
    var tblid  = $(e).attr('data-tblid')
    var folder = $(e).attr('data-folder');
    var path   = $(e).attr('data-path');
    if (id !='') {
        $.ajax({
            type   : 'post',
            url    : App.siteUrl()+'admin/delete/delete',
            data   : {'id':id,'tbl':tbl,'tblid':tblid,path:path},
            dataType : 'json',
            success:function(res) {
                if(res.status == 200 ){
                    swal('Dleted',res.msg,'success');
                    district();
                }else{
                    swal('Failed',res.msg,'error');
                }
            }

        })
    }
}
}

