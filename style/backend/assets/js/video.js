$("#news_url").keyup(function() {
    var Text = $(this).val();
    Text = Text.toLowerCase();
    Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
    $("#slug").val(Text); 

	$.ajax({
		type   : 'post',
		url    : App.siteUrl()+'admin/news/checkDuplicate',
		data   : {slug:Text},
		dataType : 'json',
		success:function(res) {
			if(res.status == 200 ){
				$('#smlpop').text(' ');
				$('#news_url').removeClass('error');
				$('#categoryForm .btn-primary' ).prop('disabled', false);
			}else{
				$('#smlpop').text(res.msg);
				$('#news_url').addClass('error');
				$('#categoryForm .btn-primary' ).prop('disabled', true);
			}
		}

	})       
});

news();
function news() {
	$('#categorytable').DataTable( { 
           	'processing': true,
            'serverSide': true,
            stateSave: true,
            "bDestroy": true,
            'responsive':true,
            "order": [[ 0, "desc" ]],
            'serverMethod': 'post',
               "ajax": {
                    "url":App.siteUrl()+'admin/videos/list',
                    "type": "POST",
                },
                "columns": [
	                {"data":"video_news_id"},
	                {"data": "title"},
	                {"data": "view"},
	                {"data": "image"},
	                {"data": "created_at"},
	                {"data":"status"}
                ],
                'columnDefs': [ {
                              
                }],
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                    return nRow;
            } 
    } );
}

function deleteNews(e){
	if(confirm('are you sure. You want to delete ')) {
		var id     = $(e).attr('data-id');
		var tbl    = $(e).attr('data-tbl');
		var tblid  = $(e).attr('data-tblid')
		var folder = $(e).attr('data-folder');
		var path   = $(e).attr('data-path');
		if (id !='') {
			$.ajax({
				type   : 'post',
				url    : App.siteUrl()+'admin/delete/delete',
				data   : {'id':id,'tbl':tbl,'tblid':tblid,path:path,'folder':folder},
				dataType : 'json',
				success:function(res) {
					if(res.status == 200 ){
						swal('Dleted',res.msg,'success');
						news();
					}
				}

			})
		}
	}
}
