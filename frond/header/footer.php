 <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Santhi</h3>
            <p>
              ponnani
              <strong>Phone:</strong> +91 5589 55488 55<br>
              <strong>Email:</strong> info@santhi.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
             
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Facilities</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Doctors</a></li>
               <li><i class="bx bx-chevron-right"></i> <a href="#">Contact Us</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">FAQ</a></li>
              
              

              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Department</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">dep</a></li>
              
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Join Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
            
            
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
          &copy; Copyright <strong><span>Santhi</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
         
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="plugin/purecounter/purecounter_vanilla.js"></script>
  <script src="plugin/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="plugin/glightbox/js/glightbox.min.js"></script>
  <script src="plugin/swiper/swiper-bundle.min.js"></script>
  <script src="plugin/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="js/main.js"></script>
  