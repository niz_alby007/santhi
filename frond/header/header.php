 <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope"></i> <a href="mailto:contact@example.com">contact@santhi.com</a> &nbsp;
        <a href="tel:9747470679"> <i class="bi bi-phone"></i>+91 5589 55488 55</a>
      </div>
      <div class="d-none d-lg-flex social-links align-items-center">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
    </div>
  </div>
  <!---------------------------header----------->
 
 <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.php">Santhi</a></h1>
     <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link  active" href="index.php">Home</a></li>
          <li><a class="nav-link  about" href="aboutus.php">About Us</a></li>
         
          <li><a class="nav-link facility" href="facilities.php">Facilities</a></li>
          
          <li><a class="nav-link doctor" href="doctors.php">Doctors</a></li>
          <li class="dropdown "><a href="departments.php" class="department"><span>Departments</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
            
              <li><a href="departments-view.php">General Medicine</a></li>
              <li><a href="departments-view.php">Neurology</a></li>
              <li><a href="departments-view.php">Ayurvedic</a></li>
            </ul>
          </li>
          
           <li><a class="nav-link scrollto gallery" href="gallery.php">Gallery</a></li>
          <li><a class="nav-link scrollto contact" href="contactus.php">Contact</a></li>
          
           <li><a class="nav-link faq" href="faq.php">FAQ</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      <a href="tel:9747470679" class="appointment-btn scrollto"><span class="d-none d-md-inline">Make an</span> Appointment</a>

    </div>
  </header>