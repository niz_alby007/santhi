  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
 <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">-->

  <!-- Vendor CSS Files -->
  <link href="plugin/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="plugin/animate.css/animate.min.css" rel="stylesheet">
  <link href="plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="plugin/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="plugin/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="plugin/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="plugin/remixicon/remixicon.css" rel="stylesheet">
  <link href="plugin/swiper/swiper-bundle.min.css" rel="stylesheet">
 <!-- Template Main CSS File -->
  <link href="css/style.css" rel="stylesheet">