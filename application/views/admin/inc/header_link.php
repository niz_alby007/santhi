
  <!-- Favicons -->
  <link href="<?=img_vlid('festival_img',get_appdata('festival_img'))?>" rel="icon">
  <link href="<?=img_vlid('festival_img',get_appdata('festival_img'))?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=base_url('style/backend/')?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url('style/backend/')?>assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">

  <link href="<?=base_url('style/backend/')?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=base_url('style/backend/')?>assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="<?=base_url('style/backend/')?>assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?=base_url('style/backend/')?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=base_url('style/backend/')?>assets/vendor/simple-datatables/style.css" rel="stylesheet">
  <link href="<?=base_url('style/backend/assets/css/')?>select2.min.css" rel="stylesheet" />

  <style>
    .open>.dropdown-menu {
    display: block;
}</style>
