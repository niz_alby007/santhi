<!-- Vendor JS Files -->
  <script src="<?=base_url('style/backend/')?>assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/chart.js/chart.umd.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/echarts/echarts.min.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/quill/quill.min.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="<?=base_url('style/backend/')?>assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->

  <script src="<?=base_url('style/backend/assets/js/')?>jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  <script src="<?=base_url('style/backend/')?>assets/js/main.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="<?=base_url('style/js/')?>app.js"></script>

  <script>
    App.init({
    site_url: '<?php echo site_url() ?>',
    base_url: '<?php echo base_url() ?>',
    user_role: '<?php echo $this->current_role ?>'
    });

  </script>

</body>

</html>