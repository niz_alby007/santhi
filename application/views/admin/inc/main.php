<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?=get_appdata('pageTitle')?></title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <?php $this->load->view('admin/inc/header_link');?>

  <!-- Template Main CSS File -->
  <link href="<?=base_url('style/backend/')?>assets/css/style.css" rel="stylesheet">
   <?php
    if ( isset($this->data['assets']['header']) && !empty($this->data['assets']['header']) ) {
        foreach ($this->data['assets']['header'] as $_h_asset) {
            $this->load->view($_h_asset);
        }
    }
    ?>
</head>

<body>
  <?php $this->load->view('admin/inc/top_header');?> <!--top header -->
  <?php $this->load->view('admin/inc/sidebar');?> <!-- sidebar -->
  <!-- body contents -->
  <?php $this->load->view('admin/inc/title_bar');?> <!-- sidebar -->
  <?php  $this->load->view("admin/{$template}");?>

  <!-- footer -->
  <?php $this->load->view('admin/inc/footer');?>
  <?php $this->load->view('admin/inc/footer_link');?> <!-- footer links and body html close -->
  <!-- addition links -->
  <?php
    if ( isset($this->data['assets']['footer']) && !empty($this->data['assets']['footer']) ) {
        foreach ($this->data['assets']['footer'] as $_f_asset) {
            $this->load->view($_f_asset);
        }
    }
  ?>

  