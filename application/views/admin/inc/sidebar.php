<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="<?=base_url('admin/home')?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#pro-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-house"></i><span>Departments</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="pro-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/departments');?>">
              <i class="bi bi-circle"></i><span>Departments List</span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/departments/create');?>">
              <i class="bi bi-circle"></i><span>Add Departments</span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#homeslide-nav" data-bs-toggle="collapse" href="#">
          <i class="bx  bx-home-heart"></i><span>Home Page</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="homeslide-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/slider');?>">
              <i class="bi bi-circle"></i><span>Slider List</span>
            </a>
          </li>

          <li>
            <a href="<?=base_url('admin/slider/create');?>">
              <i class="bi bi-circle"></i><span>Add Slider </span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#about-nav" data-bs-toggle="collapse" href="#">
          <i class="ri-service-line"></i><span>About us Page</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="about-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/about_us');?>">
              <i class="bi bi-circle"></i><span>Main About us</span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/home-page-controler');?>">
              <i class="bi bi-circle"></i><span>Home Page Data </span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

        <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#location-nav" data-bs-toggle="collapse" href="#">
          <i class="bx bx-map-alt"></i><span>Category</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="location-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/category');?>">
              <i class="bi bi-circle"></i><span>Category List</span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/category/create');?>">
              <i class="bi bi-circle"></i><span>Add Category</span>
            </a>
          </li>
          

        </ul>
      </li><!-- End Icons Nav -->


      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#agent-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-person"></i><span>Doctors</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="agent-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/doctors');?>">
              <i class="bi bi-circle"></i><span>Doctors List</span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/doctors/create');?>">
              <i class="bi bi-circle"></i><span>Add Doctors</span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#news-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-newspaper"></i><span>Gallery</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="news-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/gallery');?>">
              <i class="bi bi-circle"></i><span>Gallery List</span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/gallery/create');?>">
              <i class="bi bi-circle"></i><span>Add  Gallery</span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#developer-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-shield-check"></i><span>Facilities</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="developer-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/facilities');?>">
              <i class="bi bi-circle"></i><span>Facilities List</span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/facilities/create');?>">
              <i class="bi bi-circle"></i><span>Add Facilities</span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

       <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#tetimonials-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-person"></i><span>Testimonials</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="tetimonials-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="<?=base_url('admin/testimonials');?>">
              <i class="bi bi-circle"></i><span>Testimonials </span>
            </a>
          </li>
          <li>
            <a href="<?=base_url('admin/testimonials/create');?>">
              <i class="bi bi-circle"></i><span>Add Testimonials</span>
            </a>
          </li>
        </ul>
      </li><!-- End Icons Nav -->

    

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('admin/appdata');?>">
          <i class="bi bi-file-earmark"></i>
          <span>Appdata</span>
        </a>
      </li><!-- End Blank Page Nav -->

       <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('admin/logout')?>">
          <i class="bi bi-box-arrow-in-right"></i>
          <span>Logged out</span>
        </a>
      </li><!-- End Login Page Nav -->


    </ul>

  </aside><!-- End Sidebar-->
  <main id="main" class="main">