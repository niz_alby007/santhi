<?php 
if(!empty($editdata)){
  $id        = encryptor($editdata->id);
  $category  = $editdata->category;
  $slug      = $editdata->slug;
}else{
  $id=$category=$slug='';
}
?>
    <section class="section">
      <div class="row">
        <div class="col-sm-12"> 

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?=$pageTitle;?></h5>

              <!-- General Form Elements -->
              <form id="mainAboutForm" enctype="multipart/form-data" class="row g-3 needs-validation" method="post" action="<?=base_url('admin/about_us/main_submit')?>" novalidate>
              <?=messages();?>
          
              <div class="row mb-2">
                  <!-- <label for="validationCustom02"  class="col-sm-12 col-form-label">About us</label> -->
                  <div class="col-sm-12">
                  <textarea class="form-control"  name="about_description" rows="3"><?=get_appdata('about_description')?></textarea>
                    <span class="error" id="smlpop"></span>
                </div>
                </div>
                 <div class="form-group row  mb-3">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Chairman Image</label>
                    <div class="col-sm-9">
                        <input type="file" name="about_chairman_file">
                        <input type="hidden" name="chairman_oldpath" value="<?=get_appdata('about_chairman_file')?>" >
                        <img src="<?=img_vlid('aboutus',get_appdata('about_chairman_file'));?>" width="30%">
                    </div>
                </div>
              
                <div class="row mb-3">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Save Data</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->


              

            </div>
          </div>

        </div>
      </div>
      <div class="row mb-5">
        <div class="col-sm-12">
        <form id="mainAboutForm" enctype="multipart/form-data"  class="row g-3 needs-validation" method="post" action="<?=base_url('admin/about_us/main_submit')?>" novalidate>
           <div class="card">
                <ivd class="card-body">
                
                <div class="row mb-2">
                  <h5 class="card-title">Greeting</h5>
                    <div class="col-sm-12">
                        <textarea class="form-control"  name="abt_greeting" rows="3"><?=get_appdata('abt_greeting')?></textarea>
                        <span class="error" id="smlpop"></span>
                    </div>
                </div>


                <div class="row mb-2">
                  <h5 class="card-title">Greeting youtube video url</h5>
                    <div class="col-sm-12">
                        <input type="url" value="<?=get_appdata('abt_youtube')?>" class="form-control"  name="abt_youtube" rows="3" />
                        <span class="error" id="abt_youtube"></span>
                    </div>
                </div>

                <div class="form-group row  mb-3">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Greeting Image</label>
                    <div class="col-sm-9">
                        <input type="file" name="about_greeting_file">
                        <input type="hidden" name="greeting_oldpath" value="<?=get_appdata('about_greeting_file')?>" >
                        <img src="<?=img_vlid('aboutus',get_appdata('about_greeting_file'));?>" width="30%">
                    </div>
                </div>
                </div>
              </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Vission</h5>

                    
          
          

              <div class="row mb-2">
                  <!-- <label for="validationCustom02"  class="col-sm-12 col-form-label">Vission</label> -->
                    <div class="col-sm-12">
                        <textarea class="form-control"  name="abt_vission" rows="3"><?=get_appdata('abt_vission')?></textarea>
                        <span class="error" id="smlpop"></span>
                    </div>
                </div>

                <div class="form-group row  mb-3">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Vission Image</label>
                    <div class="col-sm-9">
                        <input type="file" name="about_vission_file">
                        <input type="hidden" name="vission_oldpath" value="<?=get_appdata('about_vission_file')?>" >
                        <img src="<?=img_vlid('aboutus',get_appdata('about_vission_file'));?>" width="30%">
                    </div>
                </div>

                </div>
                </div>
                <div class="card">
                <div class="card-body">
                <div class="row mb-2" >
                    <div class=" mb-3">
                    <h5 class="card-title">Mission</h5>
                        <div class="col-sm-12">
                            <textarea class="form-control"  name="abt_mission" rows="3"><?=get_appdata('abt_mission')?></textarea>
                            <span class="error" id="smlpop"></span>
                        </div>
                    </div>

                    <div class="form-group row  mb-3">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">Mission Image</label>
                        <div class="col-sm-9">
                            <input type="file" name="about_mission_file">
                            <input type="hidden" name="mission_oldpath" value="<?=get_appdata('about_mission_file')?>" >
                            <img src="<?=img_vlid('aboutus',get_appdata('about_mission_file'));?>" width="30%">
                        </div>
                    </div>
                </div>
                </div>
                </div>

               
              <div class="card">
                <div class="card-body">
                
                <div class="row mb-2">
                  <h5 class="card-title">Seo </h5>

                    <div class="col-sm-12 mb-3">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Meta Keywords </label>
                        <input type="text" class="form-control"  name="abt_meta_key"  value="<?=get_appdata('abt_meta_key')?>">
                        <span class="error" id="smlpop"></span>
                    </div>

                    <div class="col-sm-12 mb-3">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Meta Description</label>
                        <input type="text" class="form-control"  name="abt_meta_description"  value="<?=get_appdata('abt_meta_description')?>">
                        <span class="error" id="smlpop"></span>
                    </div>
                </div>
                
              
                <div class="row mb-3">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Save Data</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

                </div>
            </div>
        </div>
      </div>
    </section>




