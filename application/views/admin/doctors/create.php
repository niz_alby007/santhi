<?php 
if(!empty($editdata)){
  $id             = encryptor($editdata->id);
  $name           = $editdata->name;
  $designation    = $editdata->degree_for_surgeons;
  $contact_number = $editdata->contact_number;
  $twitter        = $editdata->twitter;
  $instagram      = $editdata->instagram;
  $linkedin       = $editdata->linkedin;
  $facebook       = $editdata->facebook;
  $img            = $editdata->path;
  $short_description = $editdata->short_description;
}else{
  $id=$name=$designation=$img=$contact_number=$facebook=$instagram=$linkedin=$twitter=$short_description='';
}
?>
    <section class="section">
      <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
         
                <h5 class="card-title"><?=$pageTitle;?></h5>

              <!-- General Form Elements -->
              <form id="doctorsForm" class="row g-3 needs-validation" method="post" action="<?=base_url('admin/doctors/submit')?>" novalidate enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?=$id;?>">
                <input type="hidden" name="path" value="<?=$img?>" class="form-control">

                <div class="col-sm-12 mb-3">
                    <div class="row">
                        <label for="title"  class="col-sm-12 col-form-label">Profile [600 * 600 ]</label>
                        <div class="col-sm-10">
                            <input type='file' name="file" class="form-control border" onchange="readURL(this);" />
                            <span class="error" id=""></span>
                        </div>
                        <div class="col-sm-2">
                            <?php
                                if(!empty($img)){?>
                                    <img id="blah" src="<?=img_vlid('doctors',$img)?>" width="150px" alt="" />
                                <?php }else { ?>
                                    <img id="blah" src="<?=base_url('uploads/default.png')?>" width="150px" alt="" />
                                <?php } ?>
                        </div>
                    </div>
                </div> 

                <div class="row mb-3">
                   <div class="col-sm-6  mb-3">
                <label for="name"  class="col-sm-12 col-form-label">Name</label>
                <div class="col-sm-12">
                  <input type="text"  name="name"  value="<?=$name;?>" class="form-control" id="name" required>
                  <span class="error" id=""></span>
                </div>
              </div>
              

                <div class="col-sm-6  ">
                  <label for="validationCustom33"  class="col-sm-12 col-form-label">Degree for surgeons</label>
                  <div class="col-sm-12">
                  <input type="text"  name="designation"  value="<?=$designation;?>" class="form-control"  required>                    
                  <span class="error" id=""></span>
                </div>
                </div>
                  </div>
                <div class="row mb-3">
                  <label for="validationCustom33"  class="col-sm-12 col-form-label">Short Description</label>
                  <div class="col-sm-12">
                  <textarea  name="short_description"  rows="5" class="form-control"  ><?=$short_description;?></textarea>
                  <span class="error" id=""></span>
                </div>
                </div>

                  <div class="row mb-3">
                  <label for="inputText"  class="col-sm-2 col-form-label">Department</label>
                  <div class="col-sm-10">
                  <select class="form-control select2 property_type_ct" name="department" id="department" style="text-transform:uppercase" >
                    <option value="" > ~~ Select Department ~~ </option>
                    <?php $listCategory = __category();
                          foreach($listCategory as $cate) { ?>
                          <option value="<?=$cate->category_id;?>" ><?=$cate->category;?></option>
                    <?php } ?>
                  </select>
                  </div>
                </div>

                 <div class="row mb-3">
                  <label for="validationCustom0202"  class="col-sm-2 col-form-label">Contact No</label>
                  <div class="col-sm-10">
                  <input type="text"  name="contact_number"  value="<?=$contact_number;?>" class="form-control"  >                    
                  <span class="error" id=""></span>
                </div>
                </div>

                <div class="row mb-3">
                  <label for="validationCustom0202"  class="col-sm-2 col-form-label">Twitter</label>
                  <div class="col-sm-10">
                  <input type="url"  name="twitter"  value="<?=$twitter;?>" class="form-control"  >                    
                  <span class="error" id=""></span>
                </div>
                </div>

                <div class="row mb-3">
                  <label for="validationCustom02"  class="col-sm-2 col-form-label">Facebook</label>
                  <div class="col-sm-10">
                  <input type="url"  name="facebook"  value="<?=$facebook;?>" class="form-control" id="" > 
                    <span class="error" id=""></span>
                </div>
                </div>

                <div class="row mb-3">
                  <label for="validationCustom03"  class="col-sm-2 col-form-label">Linked in</label>
                  <div class="col-sm-10">
                  <input type="url"  name="linkedin"  value="<?=$linkedin;?>" class="form-control" id="" >                    
                  <span class="error" id=""></span>
                </div>
                </div>

                <div class="row mb-3">
                  <label for="validationCustom03"  class="col-sm-2 col-form-label">Instagram</label>
                  <div class="col-sm-10">
                  <input type="url"  name="instagram"  value="<?=$instagram;?>" class="form-control" id="" >                    
                  <span class="error" id=""></span>
                </div>
                </div>

                
              
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>
      </div>
     
    </section>
