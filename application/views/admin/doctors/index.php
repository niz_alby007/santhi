<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />

<section class="section">
    <div class="row">
      <div class="col-12">
            <div class="card recent-sales overflow-auto">
              <div class="card-body">
                <h5 class="card-title"><?=$pageTitle;?></h5>

                <table id="doctordTbl" class="table table-borderless ">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>
                      <th scope="col">Designation</th>
                      <th scope="col">Image</th>
                      <th scope="col">Contact Number</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  
                </table>

              </div>

            </div>
          </div><!-- End Recent Sales -->
    </div>
  </section>



