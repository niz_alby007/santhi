

    <section class="section">
      <div class="row">
        <div class="col-sm-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?=$pageTitle;?></h5>

              <!-- General Form Elements -->
              <form method="post" action="<?=base_url('admin/appdata/submit')?>" enctype="multipart/form-data">
                <?=messages();?>
                <div class="row mb-3">
                  <label for="inputText"  class="col-sm-2 col-form-label">Page Title</label>
                  <div class="col-sm-10">
                    <input type="text" value="<?=get_appdata('pagetitle')?>" class="form-control" name="pagetitle">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" name="site_email" value="<?=get_appdata('site_email')?>">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Contact No</label>
                  <div class="col-sm-10">
                    <input type="text" name="contact_number" value="<?=get_appdata('contact_number')?>" class="form-control">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Tel No</label>
                  <div class="col-sm-10">
                    <input type="text" name="tel_no" value="<?=get_appdata('tel_no')?>" class="form-control">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Address</label>
                  <div class="col-sm-10">
                    <input type="text" name="site_address" value="<?=get_appdata('site_address')?>" class="form-control">
                  </div>
                </div>

                <div class="col-sm-12 mb-3">
                  <h5 class="card-title">Footer Content</h5>
                  <textarea class="form-control"  name="footer_content" rows="3"><?=get_appdata('footer_content')?></textarea>
                </div>

              
             

                             


              <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Facebook</label>
                  <div class="col-sm-10">
                    <input type="text" name="facebook" value="<?=get_appdata('facebook')?>" class="form-control">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Instagram</label>
                  <div class="col-sm-10">
                    <input type="text" name="insta" value="<?=get_appdata('insta')?>" class="form-control">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Linked in</label>
                  <div class="col-sm-10">
                    <input type="text" name="linkedin" value="<?=get_appdata('linkedin')?>" class="form-control">
                  </div>
                </div>

               
                  <div class="row mb-3">
                     <label class="col-sm-2 col-form-label" for="exampleInputEmail1">Fav Icon</label>
                    <div class="col-sm-10">
                    <input type="hidden" name="old_festival_img" value="<?=get_appdata('festival_img')?>">
                    <input type="file" name="festival_img"  class="form-control" >
                     <img src="<?=img_vlid('festival_img',get_appdata('festival_img'))?>" style="width: 30px;float: right;">
                  </div>
               </div>

               


                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label">Submit Button</label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit Form</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>

        
      </div>
    </section>




