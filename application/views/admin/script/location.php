 <script src="<?=base_url('style/backend/')?>assets/js/main.js"></script>
 <script src="<?=base_url('style/backend/assets/js/')?>jquery.validate.min.js"></script>


 <script>

 	$(function() {

  $("#locationForm").validate({
    rules: {
      district: {
        required: true,
      },
      Descrizione: {
        required: true
      }
    },
    messages: {
      district: {
        required: "Please enter some data"
      },
      Descrizione: {
        required: "Please provide some data"
      }
    },
    submitHandler: function(form,e) {
            e.preventDefault();
            $('#locationForm .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
            $('#locationForm .btn-primary' ).prop('disabled', true);

            $.ajax({
            	method : form.method,
            	url    : form.action,
            	data   : $('form').serialize(),
            	dataType : 'json',
            	success:function(res) {
            		if(res.status == 200) {
            			swal("Good job!", res.msg, "success");
            			setTimeout(function() {
            				$('#locationForm .btn-primary' ).html('Submit');
            				$('#locationForm .btn-primary' ).prop('disabled', false);
            				$('#locationForm').trigger("reset");
            			}, 200);

            		}else{
            			$('#locationForm .btn-primary' ).html('Submit');
            			swal(res.msg,'error');
            		}
            	}

            })
            return false;
        }
  });
});


</script>