<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url('style/backend/')?>assets/js/main.js"></script>
<script src="<?=base_url('style/backend/assets/js/')?>jquery.validate.min.js"></script>
<script src="<?=base_url('style/backend/assets/js/')?>testimonials.js"></script>

<script>


   $(function() {

$("#serviceeditor").validate({
 
  rules: {
    title: {
      required: true,
    },
    'servicepoint[]': {
     //required: true
    }
  },
  messages: {
    title: {
      required: "Please enter some data"
    },
  },
  submitHandler: function(form,e) {
          e.preventDefault();
          $('#serviceeditor .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
          $('#serviceeditor .btn-primary' ).prop('disabled', true);
          formData =  new FormData(document.getElementById('serviceeditor'));

          $.ajax({
              method : form.method,
              url    : form.action,
              data   : formData,
            contentType: false,
            processData: false,
              dataType : 'json',
              success:function(res) {
                  if(res.status == 200) {
                      swal("Good job!", res.msg, "success");
                      setTimeout(function() {
                          $('#serviceeditor .btn-primary' ).html('Submit');
                          $('#serviceeditor .btn-primary' ).prop('disabled', false);
                          window.location.href = "<?=base_url('admin/testimonials');?>";
                      }, 200);

                  }else{
                      $('#serviceeditor .btn-primary' ).html('Submit');
                      swal('oops',res.msg,'error');
                  }
              }

          })
          return false;
      }
});
});


function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
      $('#blah').attr('src', e.target.result);
  };
    reader.readAsDataURL(input.files[0]);
  }
}

</script>