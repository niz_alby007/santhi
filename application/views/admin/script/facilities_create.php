<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.ckeditor.com/4.20.2/standard/ckeditor.js"></script>

<script src="<?=base_url('style/backend/multi/script.js');?>"></script>
<script src="<?=base_url('style/backend/multi/light.js');?>"></script>
<script src="<?=base_url('style/backend/assets/js/')?>select2.min.js"></script>
<script src="<?=base_url('style/backend/assets/js/')?>jquery.validate.min.js"></script>


<script>


$(document).ready(function() {
    $('.department_by_doctors').select2();
});






$(function() {

    $("#departmentForm").validate({
    
    rules: {
        short_description: {
        required: true,
        },
        <?php 
        if(empty($editdata)){ ?>
        file:{
            required:true,
        },
        <?php } ?>
       
    },
    messages: {
        
    },
        submitHandler: function(form,e) {
            e.preventDefault();
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $('#departmentForm .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
            $('#departmentForm .btn-primary' ).prop('disabled', true);
            formData =  new FormData(document.getElementById('departmentForm'));

            $.ajax({
                method : form.method,
                url    : form.action,
                data   : formData,
                contentType: false,
                processData: false,
                dataType : 'json',
                success:function(res) {
                    if(res.status == 200) {
                        swal("Good job!", res.msg, "success");
                        setTimeout(function() {
                            $('#departmentForm .btn-primary' ).html('Submit');
                            $('#departmentForm .btn-primary' ).prop('disabled', false);
                            $('#departmentForm').trigger("reset"); 
                            window.location.href = "<?=base_url('admin/facilities');?>";
                        }, 200);

                    }else{
                        $('#departmentForm .btn-primary' ).html('Submit');
                        swal('oops',res.msg,'error');
                    }
                }

            })
            return false;
        }
    });
});






CKEDITOR.replace('doc_description',{ } );
 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
    };
      reader.readAsDataURL(input.files[0]);
    }
  }


function deleteProImg(e){
  if(confirm('are you sure. You want to delete ')) {
    var id     = $(e).attr('data-id');
    var tbl    = $(e).attr('data-tbl');
    var tblid  = $(e).attr('data-tblid')
    var folder = $(e).attr('data-folder');
    var path   = $(e).attr('data-path');
    if (id !='') {
      $.ajax({
        type   : 'post',
        url    : App.siteUrl()+'admin/delete/delete',
        data   : {'id':id,'tbl':tbl,'tblid':tblid,path:path,'folder':folder},
        dataType : 'json',
        success:function(res) {
          if(res.status == 200 ){
            swal('Dleted',res.msg,'success');
          }
        }

      })
    }
  }
}

<?php
   // if(empty($editdata->slug)){?>
    
    $("#page_link").keyup(function() {
      var Text = $(this).val();
      Text = Text.toLowerCase();
      Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
      $("#slug").val(Text);  
        $.ajax({
        type   : 'post',
        url    : App.siteUrl()+'admin/headers/checkDuplicate_fici',
        data   : {slug:Text},
        dataType : 'json',
        success:function(res) {
            if(res.status == 200 ){
                $('#smlpop').text(' ');
                $('#page_link').removeClass('error');
                $('#blogForm .btn-primary' ).prop('disabled', false);
            }else{
                $('#smlpop').text(res.msg);
                $('#page_link').addClass('error');
                $('#blogForm .btn-primary' ).prop('disabled', true);
            }
        }

    })
    
    });
<?php //} ?>

</script>
