<script src="<?=base_url('style/backend/')?>assets/js/main.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url('style/backend/assets/js/category.js')?>"></script>
<script src="<?=base_url('style/backend/assets/js/')?>jquery.validate.min.js"></script>


<script>

    $(function() {

 $("#categoryForm").validate({
   rules: {
     category: {
       required: true,
     },
     Descrizione: {
       required: true 
     }
   },
   messages: {
    category: {
       required: "Please enter some data"
     },
     Descrizione: {
       required: "Please provide some data"
     }
   },
   submitHandler: function(form,e) {
           e.preventDefault();
           $('#categoryForm .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
           $('#categoryForm .btn-primary' ).prop('disabled', true);

           $.ajax({
               method : form.method,
               url    : form.action,
               data   : $('form').serialize(),
               dataType : 'json',
               success:function(res) {
                   if(res.status == 200) {
                       swal("Good job!", res.msg, "success");
                       setTimeout(function() {
                           $('#categoryForm .btn-primary' ).html('Submit');
                           $('#categoryForm .btn-primary' ).prop('disabled', false);
                           $('#categoryForm').trigger("reset");
                       }, 200);

                   }else{
                       $('#categoryForm .btn-primary' ).html('Submit');
                       swal(res.msg,'error');
                   }
               }

           })
           return false;
       }
 });
});


</script>