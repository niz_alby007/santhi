<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url('style/backend/')?>assets/js/main.js"></script>
<script src="<?=base_url('style/backend/assets/js/')?>jquery.validate.min.js"></script>

<script>

<?php if(!empty($editdata)){?> 
        $('#department').val('<?=$editdata->department;?>').trigger('change');
<?php } ?>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
    };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(function() {

    $("#doctorsForm").validate({
    
    rules: {
        name: {
        required: true,
        },
        designation:{
            required:true,
        },
        <?php 
        if(empty($editdata)){ ?>
        file:{
            required:true,
        },
        <?php } ?>
        department:{
            required:true,
        },
         short_description:{
            required:true,
        },
    },
    messages: {
        
    },
        submitHandler: function(form,e) {
            e.preventDefault();
            $('#doctorsForm .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
            $('#doctorsForm .btn-primary' ).prop('disabled', true);
            formData =  new FormData(document.getElementById('doctorsForm'));

            $.ajax({
                method : form.method,
                url    : form.action,
                data   : formData,
                contentType: false,
                processData: false,
                dataType : 'json',
                success:function(res) {
                    if(res.status == 200) {
                        swal("Good job!", res.msg, "success");
                        setTimeout(function() {
                            $('#doctorsForm .btn-primary' ).html('Submit');
                            $('#doctorsForm .btn-primary' ).prop('disabled', false);
                            //$('#serviceeditor').trigger("reset");
                        }, 200);

                    }else{
                        $('#doctorsForm .btn-primary' ).html('Submit');
                        swal('oops',res.msg,'error');
                    }
                }

            })
            return false;
        }
    });
});


</script>
