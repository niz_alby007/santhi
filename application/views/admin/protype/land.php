 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');


  // $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land='';
  //   $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;

 if(!empty($edit)){
$propertyId = $edit->properties_id;
    $id = $edit->properties_id;
    $property_name = $edit->property_name;
    $property_type = $edit->property_type;
    $bathroom = $edit->bathrooms;
    $construction_status = $edit->construction_status;
    $area_ft = $edit->area_ft;
    $floor_no = $edit->floor_no;
    $maintenance_duration = $edit->maintenance_duration;
    $maintenance_fee = $edit->maintenance_fee;
    $bedrooms = $edit->bedrooms;
    $carpet_area = $edit->carpet_area;
    $furnished_ststus = $edit->furnished_ststus;
    $car_parking = $edit->car_parking;
    $amount = $edit->amount;
    $category = $edit->category;
    $description = $edit->description;
    $area = $edit->area;
    $ft_price = $edit->ft_price;
    $facing   = $edit->facing;
    $total_floor = $edit->total_floor;
    $ploat_length =$edit->ploat_length;
    $ploat_breadth =$edit->ploat_breadth;
    $landtype       = $edit->landtype;
    $family_status = $edit->family_status;
    $available_from = $edit->available_from;
  }else{
    $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land='';
    $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;
  }

    ?>


 <div  id="ploatDtl">
            <div class="row" id="ploatDtl">
           <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
              <label class="col-form-label" for="inputSuccess"> Ploat Area</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text fnt11">Land </span>
                  </div>
                    <input type="text" class="form-control " name="ploat_area" id="inputSuccess" value="<?=$area?>" placeholder="Enter ...">
                  <div class="input-group-prepend">
                    <span class="input-group-text fnt11">Measure  </span>
                  </div>
                    <select class="form-control select2 " name="landtype" id="" >
                         <option <?php if($landtype =="0"){ echo "selected"; }?> value="0" > None </option>
                              <option <?php if($landtype =="square_feet"){ echo "checked"; }?> value="square_feet" > Square  Feet </option>
                              <option <?php if($landtype =="Cent"){ echo "selected"; }?> value="Cent" >Cent</option>
                              <option <?php if($landtype =="acre"){ echo "selected"; }?> value="acre" >Acre</option>
                              <option <?php if($landtype =="hectare"){ echo "selected"; }?> value="Hectare" >Hectare</option>

                      </select>



                </div>
          </div>
        </div>

          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
              <label class="col-form-label" for="inputSuccess"> Ploat Details</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text fnt11">LENGTH </span>
                  </div>
                    <input type="text" class="form-control " name="ploat_length" id="inputSuccess" value="<?=$ploat_length?>" placeholder="Enter ...">
                  <div class="input-group-prepend">
                    <span class="input-group-text fnt11">BREADTH  </span>
                  </div>
                  <input type="text" class="form-control " name="ploat_breadth" id="inputSuccess" value="<?=$ploat_breadth?>" placeholder="Enter ...">



                </div>
          </div>
          </div>


          </div>
        <div class="row">
          <div class="col-sm-12 col-md-4 col-lg-4 high-select2" id="facingwarap">
            <div class="form-group">
              <label class="col-form-label" for="inputSuccess"> Facing </label>
                  <select name="ploat_facing" id="" class="form-control select2 facing">
                      <option <?php if($facing == "east"){echo "selected";}?> value="east">East</option>
                        <option <?php if($facing == "west"){echo "selected";}?> value="west">West</option>
                        <option <?php if($facing == "North"){echo "selected";}?> value="North">North</option>
                        <option <?php if($facing == "South"){echo "selected";}?> value="South">South</option>
                  </select>
              </div>
            </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="form-group">
                <label class="col-form-label" for="inputSuccess"> Price per YD²</label>
                <input type="text" class="form-control " name="ploat_ft_price" value="<?=$ft_price?>" id="inputSuccess" placeholder="Enter ...">
            </div>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4">
            <div class="form-group">
              <label class="col-form-label" for="inputSuccess"> Available From </label>
              <input type="date" class="form-control " name="available_from" value="<?=$available_from?>"  id="inputSuccess" placeholder="Enter ...">
              </div>
            </div>
      </div>

      </div><!-- ploat or land -->