 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');


  // $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land='';
  //   $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;

 if(!empty($edit)){
$propertyId = $edit->properties_id;
    $id = $edit->properties_id;
    $property_name = $edit->property_name;
    $property_type = $edit->property_type;
    $bathroom = $edit->bathrooms;
    $construction_status = $edit->construction_status;
    $area_ft = $edit->area_ft;
    $floor_no = $edit->floor_no;
    $maintenance_duration = $edit->maintenance_duration;
    $maintenance_fee = $edit->maintenance_fee;
    $bedrooms = $edit->bedrooms;
    $carpet_area = $edit->carpet_area;
    $furnished_ststus = $edit->furnished_ststus;
    $car_parking = $edit->car_parking;
    $amount = $edit->amount;
    $category = $edit->category;
    $description = $edit->description;
    $area = $edit->area;
    $ft_price = $edit->ft_price;
    $facing   = $edit->facing;
    $total_floor = $edit->total_floor;
    $ploat_length =$edit->ploat_length;
    $ploat_breadth =$edit->ploat_breadth;
    $landtype       = $edit->landtype;
    $family_status = $edit->family_status;
    $available_from = $edit->available_from;
  }else{
    $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land=$construction_status='';
    $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;
  }

    ?>

 
 <idv class="guestHouseAppartment ">
              <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">
                        <label class="col-form-label" for="inputSuccess"> Bedrooms</label>
                        <input type="text" class="form-control " name="bedrooms" value="<?=$bedrooms?>" id="inputSuccess" placeholder="Enter ...">
                      </div>
            </div>
           <div class="col-sm-12 col-md-6 col-lg-6">

                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Bathrooms</label>
                          <input type="text" class="form-control " name="bathrooms" value="<?=$bathroom;?>" id="inputSuccess" placeholder="Enter ...">
                        </div>
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Furnished  Status</label>
                        </div>


                        <div class="form-group clearfix">


                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary9" value="furnished" <?php if($furnished_ststus == "furnished"){ echo 'checked';}?> name="furnished_ststus">
                            <label for="radioPrimary9">
                              Furnished
                            </label>
                          </div>
                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary10" value="semi furnished" <?php if($furnished_ststus == "semi furnished"){ echo 'checked';}?> name="furnished_ststus" >
                            <label for="radioPrimary10">
                              semi furnished
                            </label>
                          </div>

                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary11" value="unfurnished" <?php if($furnished_ststus == "unfurnished"){ echo 'checked';}?> name="furnished_ststus" >
                            <label for="radioPrimary11">
                              unfurnished
                            </label>
                          </div>
                        </div>
              </div>


              <div class="col-sm-12 col-md-6 col-lg-6" id="maintanceWrap">
                <div class="form-group">
                  <label class="col-form-label" for="inputSuccess"> Maintenance </label>
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text fnt11">Maintenance </span>
                      </div>
                      <select name="maintenance_duration" id="" class="form-control select2 maintenance_duration">
                          <option <?php if($maintenance_duration == "0"){echo "selected";}?> value="0">None</option>
                        <option <?php if($maintenance_duration == "per_day"){echo "selected";}?> value="per_day" >Per Day</option>
                        <option <?php if($maintenance_duration == "weekly"){echo "selected";}?> value="weekly" >Weekly</option>
                        <option <?php if($maintenance_duration == "monthly"){echo "selected";}?> value="monthly" >Monthly</option>
                        <option <?php if($maintenance_duration == "yearly"){echo "selected";}?> value="yearly" >Yearly</option>


                      </select>
                         <div class="input-group-prepend">
                      <span class="input-group-text ">₹ </span>
                      </div>

                      <!-- <input type="text" class="form-control" placeholder="First Name"> -->
                      <input type="text" class="form-control" name="maintenance_fee" value="<?=$maintenance_fee?>" placeholder="Maintenance Fee">
                      </div>
              </div>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3">
         <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Super Builtup area (ft²)</label>
                          <input type="text" class="form-control " name="area_ft" value="<?=$area_ft?>" id="inputSuccess" placeholder="Enter ...">
                        </div>
            </div>
            
             <div class="col-sm-12 col-md-3 col-lg-3">
              <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Carpet Area (ft²)</label>
                          <input type="text" class="form-control " name="carpet_area" id="inputSuccess" value="<?=$carpet_area?>" placeholder="Enter ...">
                        </div>
            </div>

           
              </div>
              
              <div class="row">

               <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="form-group">
                 <label class="col-form-label" for="inputSuccess"> Floor Details</label>
                   <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text fnt11">Floor No </span>
                      </div>
                          <input type="text" class="form-control " name="floor_no" value="<?=$floor_no?>" id="inputSuccess" placeholder="Enter ...">
                        <div class="input-group-prepend">
                      <span class="input-group-text fnt11">Total Floors </span>
                      </div>
                       <input type="text" class="form-control " name="total_floor" value="<?=$total_floor?>" id="inputSuccess" placeholder="Enter ...">
                      </div>
                    </div>
              </div>

              <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Car Parking</label>
                          <input type="text" class="form-control " name="car_parking" value="<?=$car_parking?>" id="inputSuccess" placeholder="Enter ...">
                        </div>
              </div>
              
               <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="form-group">
                  <label class="col-form-label" for="inputSuccess"> Available From</label>
                  <input type="date" class="form-control " name="available_from" value="<?=$available_from?>" id="inputSuccess" placeholder="Enter ...">
                 </div>
               </div>


            </div>


        </idv>
