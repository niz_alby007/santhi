 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');


  // $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land='';
  //   $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;

 if(!empty($edit)){
$propertyId = $edit->properties_id;
    $id = $edit->properties_id;
    $property_name = $edit->property_name;
    $property_type = $edit->property_type;
    $bathroom = $edit->bathrooms;
    $construction_status = $edit->construction_status;
    $area_ft = $edit->area_ft;
    $floor_no = $edit->floor_no;
    $maintenance_duration = $edit->maintenance_duration;
    $maintenance_fee = $edit->maintenance_fee;
    $bedrooms = $edit->bedrooms;
    $carpet_area = $edit->carpet_area;
    $furnished_ststus = $edit->furnished_ststus;
    $car_parking = $edit->car_parking;
    $amount = $edit->amount;
    $category = $edit->category;
    $description = $edit->description;
    $area = $edit->area;
    $ft_price = $edit->ft_price;
    $facing   = $edit->facing;
    $members  = $edit->members;
    $total_floor = $edit->total_floor;
    $ploat_length =$edit->ploat_length;
    $ploat_breadth =$edit->ploat_breadth;
    $landtype       = $edit->landtype;
    $family_status = $edit->family_status;
    $available_from = $edit->available_from;
  }else{
    $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land=$construction_status=$members='';
    $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;
  }

    ?>

 
 <div class="pg-hostel" id="">
                            <div class="row">
                              <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                          <label class="col-form-label" for="inputSuccess"> Furnished  Status</label>
                                        </div>


                                        <div class="form-group clearfix">
                                          <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary11" value="furnished" <?php if($furnished_ststus == "furnished"){ echo 'checked';}?> name="furnished_ststus">
                                            <label for="radioPrimary11">
                                               Furnished
                                            </label>
                                          </div>

                                          <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary22" value="semi furnished" <?php if($furnished_ststus == "semi furnished"){ echo 'checked';}?> name="furnished_ststus" >
                                            <label for="radioPrimary22">
                                              semi furnished
                                            </label>
                                          </div>

                                          <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary33" value="unfurnished" <?php if($furnished_ststus == "unfurnished"){ echo 'checked';}?> name="furnished_ststus" >
                                            <label for="radioPrimary33">
                                              unfurnished
                                            </label>
                                          </div>
                                        </div>
                              </div>
                              <div class="col-sm-12 col-md-6 col-lg-6">
                                 <div class="form-group">
                                    <label class="col-form-label" for="inputSuccess"> Washrooms</label>
                                    <input type="text" class="form-control " name="pg" value="<?=$bathroom;?>" id="inputSuccess" placeholder="Enter ...">
                                  </div>
                               </div>

                               <div class="col-sm-12 col-md-6 col-lg-6">
                                 <div class="form-group">
                                     <label class="col-form-label" for="inputSuccess"> Car Parking</label>
                                     <input type="text" class="form-control " name="car_parking" value="<?=$car_parking?>" id="inputSuccess" placeholder="Enter ...">
                                   </div>
                               </div>
                              </div>
                              <div class="row">

                                <div class="col-sm-12 col-md-4 col-lg-4 high-select2 " id="">
                                  <div class="form-group">
                                    <label class="col-form-label" for="inputSuccess"> Meals included </label>
                                        <select name="family_status" id="family_status" class="form-control select2">
                                          <option <?php if($family_status == 1 ){echo "selected"; }?>  value="1">Yes</option>
                                          <option  <?php if($family_status == 2 ){echo "selected"; }?> value="2">No</option>
                                        </select>
                                    </div>
                                  </div>

                                  <div class="col-sm-12 col-md-4 col-lg-4">
                                    <div class="form-group">
                                      <label class="col-form-label" for="inputSuccess"> Members</label>
                                      <input type="number" class="form-control " name="members" value="<?=$members;?>" id="inputSuccess" placeholder="Members ...">
                                     </div>
                                   </div>

                                   <div class="col-sm-12 col-md-4 col-lg-4">
                                     <div class="form-group">
                                       <label class="col-form-label" for="inputSuccess"> Available From</label>
                                       <input type="date" class="form-control " name="available_from" value="<?=$available_from?>" id="inputSuccess" placeholder="Enter ...">
                                      </div>
                                    </div>

                              </div>

                        </div><!--pg / Hostel  -->