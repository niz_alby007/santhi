 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');


  // $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land='';
  //   $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;

 if(!empty($edit)){
  
    $propertyId       = $edit->properties_id;
    $id               = $edit->properties_id;
    $property_name    = $edit->property_name;
    $property_type    = $edit->property_type;
    $bathroom         = $edit->bathrooms;
    $construction_status = $edit->construction_status;
    $area_ft          = $edit->area_ft;
    $floor_no         = $edit->floor_no;
    $maintenance_duration = $edit->maintenance_duration;
    $maintenance_fee  = $edit->maintenance_fee;
    $bedrooms         = $edit->bedrooms;
    $carpet_area      = $edit->carpet_area;
    $furnished_ststus = $edit->furnished_ststus;
    $car_parking      = $edit->car_parking;
    $amount           = $edit->amount;
    $category         = $edit->category;
    $description      = $edit->description;
    $area             = $edit->area;
    $ft_price         = $edit->ft_price;
    $facing           = $edit->facing;
    $total_floor      = $edit->total_floor;
    $ploat_length     = $edit->ploat_length;
    $ploat_breadth    = $edit->ploat_breadth;
    $landtype         = $edit->landtype;
    $family_status    = $edit->family_status;
    $available_from   = $edit->available_from;
  }else{
    $id = $family_status = $propertyId =$property_name = $landtype =$property_type = $available_from = $bathroom = $construction_statu = $area_ft = $floor_no = $maintenance_duration =  $bedrooms = $carpet_area = $furnished_ststus = $car_parking = $amount = $category = $description =$ft_price = $facing = $total_floor= $measure_land=$construction_status='';
    $area= $maintenance_fee =   $ploat_breadth = $ploat_length=   0;
  }

    ?>


<div class="cateAppartment" id="cateAppartment">
            <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">
                        <label class="col-form-label" for="inputSuccess"> Bedrooms</label>
                        <input type="text" class="form-control " name="bedrooms" value="<?=$bedrooms?>" id="inputSuccess" placeholder="Enter ...">
                      </div>
            </div>
           <div class="col-sm-12 col-md-6 col-lg-6">

                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Bathrooms</label>
                          <input type="text" class="form-control " name="bathrooms" value="<?=$bathroom;?>" id="inputSuccess" placeholder="Enter ...">
                        </div>
              </div>

              <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Furnished  Status</label>
                        </div>


                        <div class="form-group clearfix">


                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary2" value="furnished" <?php if($furnished_ststus == "furnished"){ echo 'checked';}?> name="furnished_ststus">
                            <label for="radioPrimary2">
                              Furnished
                            </label>
                          </div>
                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary3" value="semi furnished" <?php if($furnished_ststus == "semi furnished"){ echo 'checked';}?> name="furnished_ststus" >
                            <label for="radioPrimary3">
                              semi furnished
                            </label>
                          </div>

                          <div class="icheck-primary d-inline">
                            <input type="radio" id="radioPrimary4" value="unfurnished" <?php if($furnished_ststus == "unfurnished"){ echo 'checked';}?> name="furnished_ststus" >
                            <label for="radioPrimary4">
                              unfurnished
                            </label>
                          </div>
                        </div>
              </div>


              <div class="col-sm-12 col-md-6 col-lg-6" id="maintanceWrap">
                <div class="form-group">
                  <label class="col-form-label" for="inputSuccess"> Maintenance </label>
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text fnt11">Maintenance  </span>
                      </div>
                      <select name="maintenance_duration" id="" class="form-control select2 maintenance_duration">
                        <option <?php if($maintenance_duration == "0"){echo "selected";}?> value="0">None</option>
                        <option <?php if($maintenance_duration == "per_day"){echo "selected";}?> value="per_day" >Per Day</option>
                        <option <?php if($maintenance_duration == "weekly"){echo "selected";}?> value="weekly" >Weekly</option>
                        <option <?php if($maintenance_duration == "monthly"){echo "selected";}?> value="monthly" >Monthly</option>
                        <option <?php if($maintenance_duration == "yearly"){echo "selected";}?> value="yearly" >Yearly</option>

                      </select>
                         <div class="input-group-prepend">
                      <span class="input-group-text ">₹ </span>
                      </div>

                      <!-- <input type="text" class="form-control" placeholder="First Name"> -->
                      <input type="text" class="form-control" name="maintenance_fee" value="<?=$maintenance_fee?>" placeholder="Maintenance Fee">
                      </div>
              </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
              <div class="form-group high-select2">
                        <label class="col-form-label" for="inputSuccess"> Construction Status</label>
                      <!--   <input type="text" class="form-control " name="construction_status" value="<?=$construction_statu?>" id="inputSuccess" placeholder="Enter ..."> -->
                       <select class="form-control select2 construction_status" name="construction_status" id="" >
                          <option value="" > ~~ Construction Status ~~ </option>
                          <option <?php if($construction_status == "completed"){echo "selected";}?> value="completed" >Completed</option>
                          <option <?php if($construction_status == "ready_to_move"){echo "selected";}?> value="ready_to_move" >Ready to move</option>
                          <option <?php if($construction_status == "under_construction"){echo "selected";}?> value="under_construction" >Under Construction</option>
                        </select>
                      </div>
            </div>

               <div class="col-sm-6">
                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Super Builtup area (ft²)</label>
                          <input type="text" class="form-control " name="area_ft" value="<?=$area_ft?>" id="inputSuccess" placeholder="Enter ...">
                        </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Carpet Area (ft²)</label>
                          <input type="text" class="form-control " name="carpet_area" id="inputSuccess" value="<?=$carpet_area?>" placeholder="Enter ...">
                        </div>
              </div>

              </div>
              <div class="row">

               <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                 <label class="col-form-label" for="inputSuccess"> Floor Details</label>
                   <div class="input-group mb-3">
                      <div class="input-group-prepend">
                      <span class="input-group-text fnt11">Floor No </span>
                      </div>
                          <input type="text" class="form-control " name="floor_no" value="<?=$floor_no?>" id="inputSuccess" placeholder="Enter ...">
                        <div class="input-group-prepend">
                      <span class="input-group-text fnt11">Total Floors </span>
                      </div>
                       <input type="text" class="form-control " name="total_floor" value="<?=$total_floor?>" id="inputSuccess" placeholder="Enter ...">
                      </div>
                    </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                          <label class="col-form-label" for="inputSuccess"> Car Parking</label>
                          <input type="text" class="form-control " name="car_parking" value="<?=$car_parking?>" id="inputSuccess" placeholder="Enter ...">
                        </div>
              </div>


            </div>


            <div class="row gusthosehide" id="areadtl">
              <div class="col-sm-12 col-md-6 col-lg-6 high-select2" id="facingwarap">
                <div class="form-group">
                  <label class="col-form-label" for="inputSuccess"> Facing </label>
                      <select name="facing" id="" class="form-control select2 facing">
                        <option <?php if($facing == "east"){echo "selected";}?> value="east">East</option>
                        <option <?php if($facing == "west"){echo "selected";}?> value="west">West</option>
                        <option <?php if($facing == "North"){echo "selected";}?> value="North">North</option>
                        <option <?php if($facing == "South"){echo "selected";}?> value="South">South</option>
                      </select>
                  </div>
                </div>
               <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                  <label class="col-form-label" for="inputSuccess"> Land area Details</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text fnt11">Land </span>
                      </div>
                        <input type="text" class="form-control " name="gusthouse_area" id="inputSuccess" value="<?=$area?>" placeholder="Enter ...">
                      <div class="input-group-prepend">
                        <span class="input-group-text fnt11">Measure  </span>
                      </div>
                        <!-- <select class="form-control select2" name="measure_land" id="construction_status" > -->
                            <select class="form-control select2 " name="landtype" id="" >
                              <option <?php if($landtype =="0"){ echo "selected"; }?> value="0" > None </option>
                              <option <?php if($landtype =="square_feet"){ echo "checked"; }?> value="square_feet" > Square  Feet </option>
                              <option <?php if($landtype =="Cent"){ echo "selected"; }?> value="Cent" >Cent</option>
                              <option <?php if($landtype =="acre"){ echo "selected"; }?> value="acre" >Acre</option>
                              <option <?php if($landtype =="hectare"){ echo "selected"; }?> value="Hectare" >Hectare</option>

                            </select>
                    </div>
              </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"> Price per ft²</label>
                    <input type="text" class="form-control " name="ft_price" value="<?=$ft_price?>" id="inputSuccess" placeholder="Enter ...">
                  </div>
              </div>
              <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                  <label class="col-form-label" for="inputSuccess"> Available From</label>
                  <input type="date" class="form-control " name="available_from" value="<?=$available_from?>" id="inputSuccess" placeholder="Enter ...">
                 </div>
               </div>




          </div>
    
                          <div class="col-sm-12 col-md-4 col-lg-4 high-select2 cateAppartment" id="">
                            <div class="form-group">
                              <label class="col-form-label" for="inputSuccess"> Batchelors Allowed </label>
                                  <select name="family_status" id="family_status" class="form-control select2">
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                  </select>
                              </div>
                            </div>

        </div><!--FLAT / APARTMENTS / HOUSE / VILLA -->