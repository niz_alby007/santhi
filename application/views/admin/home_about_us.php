

    <section class="section">
      <div class="row">
        <div class="col-sm-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?=$pageTitle;?></h5>

              <!-- General Form Elements -->
              <form method="post" action="<?=base_url('admin/appdata/home_submit')?>" enctype="multipart/form-data">
                <?=messages();?>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Why Choose Santhi?</label>
                  <div class="col-sm-10">
                    <textarea type="text" name="why_us" cols="5" class="form-control"><?=get_appdata('why_us')?></textarea>
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Exemplary </label>
                  <div class="col-sm-10">
                    <textarea type="text" name="exemplary" cols="5" class="form-control"><?=get_appdata('exemplary')?></textarea>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Technologies </label>
                  <div class="col-sm-10">
                    <textarea type="text" name="technologies" cols="5" class="form-control"><?=get_appdata('technologies')?></textarea>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Research </label>
                  <div class="col-sm-10">
                    <textarea type="text" name="research" cols="5" class="form-control"><?=get_appdata('research')?></textarea>
                  </div>
                </div>
             



               

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Doctors</label>
                  <div class="col-sm-10">
                    <input type="text" name="doctors" value="<?=get_appdata('doctors')?>" class="form-control">
                  </div>
                </div>

                 <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Departments</label>
                  <div class="col-sm-10">
                    <input type="text" name="departments" value="<?=get_appdata('departments')?>" class="form-control">
                  </div>
                </div>


                 <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Research Labs</label>
                  <div class="col-sm-10">
                    <input type="text" name="research_Labs" value="<?=get_appdata('research_Labs')?>" class="form-control">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="inputEmail" class="col-sm-2 col-form-label">Awards</label>
                  <div class="col-sm-10">
                    <input type="text" name="awards" value="<?=get_appdata('awards')?>" class="form-control">
                  </div>
                </div>


              


                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label">Submit Button</label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit Form</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>

        
      </div>
    </section>




