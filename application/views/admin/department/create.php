<?php 
if(!empty($editdata)){
  $id            = encryptor($editdata->id);
  $description   = $editdata->description;
  $path          = $editdata->path;
  $ph            = $editdata->contact_number;
  $short_description   = $editdata->short_description;
  $link          = $editdata->link;
  $slug          = $editdata->slug;
}else{
  $id=$description=$path=$short_description=$ph=$link=$slug='';
}
?>
<style>
  #propertyForm label{ 
    text-transform:capitalize;
}
#propertyForm .icheck-primary {
     margin-right:10px;
}
</style>
<section class="section">
      <div class="row">
        <div class="col-sm-12">
              <!-- General Form Elements -->
              <form id="departmentForm" method="post" action="<?=base_url('admin/departments/submit')?>" enctype="multipart/form-data">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><?=$pageTitle;?></h5>
                <?=messages();?>

                <div class="row mb-3">
                <div id="my-strictly-unique-vue-upload-multiple-image" style="display: flex; padding-bottom: 10px;  margin: 0 auto; justify-content: center;"></div>
                <?php if(!empty($department_images))
                { ?>
                <div class="col-sm-6">
                <!-- <h5 class="card-title">With indicators</h5> -->

              <!-- Slides with indicators -->
              <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                  <?php
                  $ol=0;
                  foreach($department_images as $proImgs)
                  { ?>
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="<?=$ol;?>" class="<?= ($ol === 0 ? 'active' : '') ?>" aria-label="Slide 1"></button>
                  <?php $ol++; 
                  }?>
                </div>
                <div class="carousel-inner">
                  <?php
                    $i=0;
                   foreach($department_images as $proImgs)
                   { ?>
                  <div class="innercarousel carousel-item <?= ($i === 0 ? 'active' : '') ?>">
                    <img src="<?=img_vlid('innerdepartment',$proImgs->images)?>" class="d-block w-100" alt="...">
                    <span onclick="deleteProImg(this)" data-id="<?=encryptor($proImgs->department_images_id);?>" data-tbl="department_images" data-folder="innerdepartment" data-path="<?=$proImgs->images;?>" data-tblid="department_images_id"  ><i class="bx bxs-trash"></i></span>
                  </div>
                  <?php 
                  $i++;}
                  ?>
             
                </div>

                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>

              </div><!-- End Slides with indicators -->

            </div>
            <?php } ?>
              </div>

                <div class="row mb-3">
                  <div class="col-sm-6">
                  <label for="inputText"  class="col-form-label">Department</label>
                  <select class="form-control select2 department_type" name="department_type" id="department_type" style="text-transform:uppercase" >
                    <option value="" > ~~ Select Department ~~ </option>
                    <?php $listCategory = __category();
                          foreach($listCategory as $cate) { ?>
                          <option value="<?=$cate->category_id;?>" ><?=$cate->category;?></option>
                    <?php } ?>
                  </select>

                  </div>

                  <div class="col-sm-12 col-md-6 col-lg-6">

                    <div class="form-group">
                      <label for="inputText"  class="col-form-label">Doctors </label>
                        <select id="department_by_doctors" required class="form-select border department_by_doctors" name="doctors[]" multiple="">
                          
                        </select>
                      <span class="error" id="doctors"></span>
                    </div>


                  </div>
                
                </div>
              </div>
            </div>

                
            <div class="row">
                <div class="col-sm-8">
                  <div class="card">
                    <div class="card-body">
                       <div class="row mb-4">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="col-form-label" for="inputSuccess"> Short Description </label>
                              <textarea  class="form-control " name="short_description"><?=$short_description?></textarea>
                              <span class="error" id="short_description"></span>
                            </div>
                          </div>

                          <div class="col-sm-12">
                            <label class="col-form-label" for=""> Description </label>
                            <div class="form-group">
                              <textarea name="doc_description" width="100%" rows="6" class="form-control" id="descriptionData"><?=$description?></textarea>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
             
              
              <div class="col-sm-4">              
              <div class="card">
                <div class="card-body">
                  <div class="col-sm-12">
                    <div class="row g-3" style="padding-top: 15px;">
                      <div class="col-sm-12 col-md-12 col-lg-12">

                        <div class="form-group">
                          <label for="inputText"  class="col-form-label">Page Link </label>
                            <input id="slug"  type="hidden" class="form-control" value="<?=$slug;?>" name="slug" />
                            <input id="page_link" type="text" required class="form-control border " value="<?=$link;?>" name="page_link" />
                          <span class="error" id="smlpop"></span>
                        </div>


                    <div class="form-group">
                      <label for="inputText"  class="col-form-label">Contact Number </label>
                        <input id="ph" required class="form-control border " value="<?=$ph;?>" name="ph" />
                      <span class="error" id="doctors"></span>
                    </div>


                  </div>

                    <div class="col-md-12">
                        <label for="inputAddress2" class="form-label col-sm-12">Cover Image [Keep Size 900 X 600 px]</label>
                          <input type='file' name="file" onchange="readURL(this);" />
                          <input type="hidden" name="oldpath" value="<?=$path?>" class="form-control">
                           <input type="hidden" name="id" value="<?=$id?>" class="form-control">
                          <?php
                          if(!empty($path)){?>
                            <img id="blah" src="<?=img_vlid('department',$path)?>" width="100%" alt="your image" />
                          <?php }else { ?>
                            <img id="blah" src="<?=base_url('uploads/default.png')?>" width="100%" alt="your image" />
                          <?php } ?>
                      </div>
                  </div>

                  <div class="row mb-3 mt-3">
                  <div class="col-sm-12">
                  <label class="col-form-label"></label>
                    <button type="submit" value="1" name="save" class="btn btn-primary">Save </button>
                  
                  </div>
                </div>
              </div>
              </div>

              </div> <!--clos sm-6-->
              </div>
            </div>
              
              </form><!-- End General Form Elements -->
        </div>
      </div>
    </section>




