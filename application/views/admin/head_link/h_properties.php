<style>
.produuct_list {
    padding: 0;
    display: flex;
    position: relative;
}
.list-wrap {
    padding: 5px;
    border: 1px #ddd solid;
    width: 100%;
    margin: 10px;
}
.list-wrap a {
    margin: 0;
    padding: 0;
    display: inherit;
    color: #706c6c;
    cursor: pointer;
}
.prohead .innerspantitle {
    font-size: 14px;
    font-weight: 700;
    color: #000;
}
.prohead .proavailable {
    width: fit-content;
    float: right;
    text-align: right;
    font-size: 12px;
    text-transform: uppercase;
    font-weight: 500;
    background: #02a11d;
    padding: 0 3px;
    color: #fff;
    border-radius: 3px;
}
.shortlistWrap {
    position: absolute;
    right: 17px;
    font-size: 22px;
    top: 34px;
    color: #17a2b8;
    cursor: pointer;
}
.popmsg {
    position: absolute;
    height: 40px;
    line-height: 40px;
    left: -132px;
    top: -38px;
    padding: 0px 10px;
    font-size: 12px;
    letter-spacing: 2px;
    min-width: 160px;
    width: 100%;
    background: #2d2d3d;
    margin-top: -20px;
    opacity: 0;
    visibility: hidden;
    transition: all 300ms linear;
    -webkit-transition: all 300ms linear;
}

.feature-img-wrap {
    width: 40%;
    float: left;
    max-width: 100%;
    padding-left: 0;
}
.feature-img-wrap img {
    max-width: 100%;
    height: auto;
    max-height: 100px;
    min-height: 100px;
    padding-right:10px;
}
.feature-img-wrap p {
    font-size: 10px;
    background: #f39201;
    width: fit-content;
    margin: 0 auto;
    color: #fff;
    font-weight: 600;
    padding: 2px 5px;
    border-radius: 2px;
    position: absolute;
    bottom: -9px;
    left: 0;
}
.bggrn {
    background: #f80d24!important;
}
.feature-details-wrap {
    width: 60%;
    float: left;
    max-width: 100%;
    padding-left: 0;
}

.proactionlist {
    position: absolute;
    right: 15px;
    height: 19px;
    top: 15px;
    width: 45%;
    background: black;
    opacity: 0;
    visibility: hidden;
    transition: all 300ms linear;
    -webkit-transition: all 300ms linear;
}

.list-wrap ul {
    padding: 0;
    margin: 0;
    list-style-type: none;
}
.list-wrap ul li {
    font-size: 15px;
    line-height: 1.3em;
}
.prolist_amount, .prolocation {
    font-size: 16px;
    font-weight: 700;
    color: #1a1a1a;
    padding: 0;
    text-transform: capitalize;
}
.list-wrap ul li i {
    padding: 0 5px 0 0;
    font-size: 15px;
}
.prolisttype {
    font-size: 16px;
    font-weight: 500;
    text-transform: capitalize;
}
.relative{
    position:relative
}
.locationlistwarap {
    width: 90%;
    clear: both;
    padding-top: 10px;
    display: block;
}
.datelistwarap {
    text-align: right;
    font-size: 12px;
    width: fit-content;
    float: right;
    line-height: 30px;
}
.smtitle {
    text-transform: capitalize;
    color: #343a40;
}
.prolisttype label {
    padding-left: 5px;
    line-height: 5px;
}
.prohead .danger {
    background: #d30000!important;
}
.prolisttype label {
    padding-left: 5px;
    line-height: 5px;
    padding-right:5px
}

.proaction {
    position: absolute;
    right: 12px;
    bottom: 12px;
    width: 30px!important;
    height: 30px;
    display: list-item;
    list-style-type: none;
}

.proaction span {
    width: 5px;
    height: 5px;
    background: #000;
    position: relative;
    display: block;
    margin: 5px 0;
    border-radius: 50%;
    text-align: right;
    margin: 3px auto;
    cursor: pointer;
}
.activeproaction {
    visibility: visible;
    opacity: 1;
    bottom: 12px;
    top: inherit;
}

.proactionlist ul {
    position: relative;
    width: 100%;
    list-style-type: none;
    padding: 0;
}

.proactionlist ul i {
    position: absolute;
    right: 0;
    width: 20px;
    height: 100%;
    line-height: 20px;
    font-size: 13px;
    color: #fff;
    text-align: center;
    cursor: pointer;
    float: right;
}
.proactionlist ul li {
    float: right;
    width: 80%;
    padding-left: 0;
    margin-right: 26px;
}
.proactionlist ul li a {
    padding: 0px 10px 0px 11px;
    width: 100%;
    display: block;
    color: #fff;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 400;
    background: linear-gradient(86deg, #02a11d, #02a11d);
}

</style>