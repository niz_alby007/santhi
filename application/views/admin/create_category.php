<?php 
if(!empty($editdata)){
  $id        = encryptor($editdata->id);
  $category  = $editdata->category;
  $slug      = $editdata->slug;
}else{
  $id=$category=$slug='';
}
?>
    <section class="section">
      <div class="row">
        <div class="col-sm-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?=$pageTitle;?></h5>

              <!-- General Form Elements -->
              <form id="categoryForm" class="row g-3 needs-validation" method="post" action="<?=base_url('admin/headers/submit')?>" novalidate>
                <input type="hidden" name="id" value="<?=$id;?>">
                <input type="hidden" id="slug" name="slug" value="<?=$slug;?>">
                <div class="row mb-3">
                  <label for="validationCustom01"  class="col-sm-2 col-form-label">Category</label>
                 
                  <div class="col-sm-10">
                    <input type="text" name="category"  value="<?=$category;?>" class="form-control" id="restaurant_Name" required>
                    <span class="error" id="smlpop"></span>
                </div>
                </div>
              
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label">Submit Button</label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit Form</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>
      </div>
    </section>




