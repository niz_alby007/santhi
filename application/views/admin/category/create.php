<?php 
if(!empty($editdata)){
  $id    = encryptor($editdata->category_id);
  $category = $editdata->category;
}else{
  $id=$category='';
}
?>
    <section class="section">
      <div class="row">
        <div class="col-sm-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?=$pageTitle;?></h5>

              <!-- General Form Elements -->
              <form id="categoryForm" class="row g-3 needs-validation" method="post" action="<?=base_url('admin/category/submit')?>" novalidate>
                <input type="hidden" name="id" value="<?=$id;?>">

                
                <div class="row mb-3">
                  <label for="validationCustom01"  class="col-sm-2 col-form-label">Category</label>
                 
                  <div class="col-sm-10">
                    <input type="text" name="category" value="<?=$category;?>" class="form-control" id="validationCustom01" required>
                </div>
                </div>
              
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label">Submit Button</label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Save Category</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

            </div>
          </div>

        </div>

        
      </div>
    </section>




