<?php 
if(!empty($editdata)){
  $id          = encryptor($editdata->id);
  $title       = $editdata->title;
  $description = $editdata->description;
  $img         = $editdata->path;
}else{
  $id=$title=$description=$img='';
}
?>
    <section class="section">
      <div class="row">
        <div class="col-sm-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title"><?=$pageTitle;?></h5>


              <!-- General Form Elements -->
              <form id="serviceeditor" class="row g-3 needs-validation" method="post" action="<?=base_url('admin/slider/submit')?>" novalidate enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?=$id;?>">
                <input type="hidden" name="path" value="<?=$img?>" class="form-control">

               
                <div class="row mb-3">
                  <label for="validationServer010"  class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10 " >
                        <input type='file' name="file" class="border" onchange="readURL(this);" />
                        <?php
                        if(!empty($img)){?>
                          <img id="blah" src="<?=img_vlid('slider',$img)?>" width="25%" alt="your image" />
                          <?php }else { ?>
                          <img id="blah" src="<?=base_url('uploads/default.png')?>" width="25%" alt="your image" />
                        <?php } ?>
                     
                    </div>
                </div>
                  
                <div class="row mb-3">
                <label for="title"  class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                  <input type="text" id="title" name="title"  value="<?=$title;?>" class="form-control" id="restaurant_Name" required>
                  <span class="error" id=""></span>
                </div>
                </div>

                <div class="row mb-3">
                  <label for="validationCustom02"  class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-10">
                    <textarea name="description" class="form-control" rows="5" required=""><?=$description;?></textarea>
                    <span class="error" id="smlpop"></span>
                </div>
                </div>

                
              
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label"></label>
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>

              </form><!-- End General Form Elements -->

        
          </div>

        </div>
      </div>
    </section>
