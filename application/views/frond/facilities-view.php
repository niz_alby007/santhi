

 <!-- ======= Hero Section ======= -->
  <section id="hero" class="sub-head d-flex align-items-center" style="background: url('<?=base_url('frond/');?>img/department/generalmedicin-bg.jpg') top center; height:80vh;">
    <div class="container">
      <h1><?=$pageTitle;?></h1>
      <p><?=(!empty($data->short_description) ? $data->short_description : '');?></p>
     
    </div>
</section><!-- End Hero -->



   <section>
    
    
    <div class="section-title">
          <h2><?=$pageTitle;?></h2>
         
        </div>

    
    
     <div class="container py-5">
    <div class="row">
    
  
  
   <?php 
      if(!empty($images))
      { ?>
<div class="col-lg-6 order-1 order-lg-2 text-center" data-aos="fade-up" data-aos-delay="200"  style="max-height:400px;">
  <div class="slides-1 swiper">
    <div class="swiper-wrapper">
      <?php
        foreach($images as $img)
        { ?>
      <div class="swiper-slide ">
        <div class="item fit-object" >
          <img src="<?=img_vlid('facilitiesslider',$img->images);?>" alt="" class="img-fluid" >
        </div>
      </div><!-- End slide item -->
        <?php 
      } ?>
    </div>
  <div class="swiper-pagination"></div>
    <div class="swiper-button-prev" style="font-size:12px; width:10px;"></div>
    <div class="swiper-button-next"></div> 
  </div>
  </div>
<?php } ?>
  
 
 <div class="col-lg-6" style="max-height:400px; overflow:auto;">
 <?=(!empty($data->description) ? $data->description : '');?>
       
   
 </div>         
  
 
          
          
  
                  
          
</div></div>
</section>


