
<!-- End Header -->

 <!-- ======= Hero Section ======= -->
  <section id="hero" class="sub-head d-flex align-items-center" style="background: url('<?=base_url('frond/');?>img/facility-bg.jpg') center center; height:80vh;">
    <div class="container">
      <h1><?=$pageTitle;?></h1>
      <!-- <p>Caption</p> -->
     
    </div>
</section><!-- End Hero -->


  <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Facilties</h2>
          <p></p>
        </div>

        <div class="row">
          <?php
           if(!empty($facilities))
           {
            foreach($facilities as $f_key)
            { ?>
              <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div class="icon-box">
                 <div class="icon col-lg-12" style="background-image: url(<?=img_vlid('facilities',$f_key->path)?>"></div>
                <h4 class="m-2"><a href=""><?=$f_key->title;?></a></h4>
                <p class="m-2"><?=$f_key->short_description;?></p>
                
                 <a href="<?=base_url('facilities-view/').$f_key->slug;?>" class="appointment-btn m-2"><i class="bx bx-chevron-right"></i></a>
               </div>
             
            </div>
          <?php 

            }
           } ?>

        </div>

      </div>
    </section><!-- End Services Section -->
  
  
   <style type="text/css">.navbar a:hover,
.navbar .facility,
.navbar .facility:focus,
.navbar li:hover>a {
  color: #1977cc;
  border-color: #1977cc;
}
</style>

