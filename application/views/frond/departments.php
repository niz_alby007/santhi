
  
 <!-- ======= Hero Section ======= -->
  <section id="hero" class="sub-head d-flex align-items-center" style="background: url('<?=base_url('frond/');?>img/department-bg.jpg') center center;">
    <div class="container">
      <h1>Our Departments</h1>
      <p>Caption</p>
     
    </div>
</section><!-- End Hero -->



   <!-- ======= department Section ======= -->
    <section id="dtp" class="dpt">
      <div class="container pb-5">

        <div class="section-title">
          <h2>Our Departments</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
          <?php 
          if(!empty($departments))
          {
            foreach($departments as $d_key)
              { $doctor = doctorsList(unserialize($d_key->doctors),'name'); ?> 

          <div class="col-lg-6 mt-4 mb-4 mt-lg-0">
            <div class="member d-flex align-items-start">
            <div class="row">
              <div class="pic col-lg-5"><img src="<?=img_vlid('department',$d_key->path);?>" class="img-fluid" alt=""></div>
              <div class="member-info col-lg-7">
                <h4><?=$d_key->category;?></h4>
                <span><?=rtrim($doctor,',');?></span>
                <p><?=$d_key->short_description;?></p> 
                <div class="social">
                 <!--<a href=""><i class="ri-twitter-fill"></i></a>
                  <a href=""><i class="ri-facebook-fill"></i></a>-->
                  <a href="https://wa.me/<?=$d_key->short_description;?>"><i class="ri-whatsapp-fill"></i></a>
                  <a href="<?=base_url('departments-view/').$d_key->slug;?>" title="More Details"> <i class="bx bx-chevron-right"></i> </a>
              
                </div>
              </div>
              </div><!-- row-->
              
            </div>
          </div>
          <?php 
          }
        }?>


        </div>

      </div>
      
       </section><!-- End Contact Section -->
       
       <style type="text/css">.navbar a:hover,
.navbar .department,
.navbar .department:focus,
.navbar li:hover>a {
  color: #1977cc;
  border-color: #1977cc;
}
</style>
