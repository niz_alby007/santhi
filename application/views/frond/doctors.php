
 <!-- ======= Hero Section ======= -->
  <section id="hero" class="sub-head d-flex align-items-center" style="background: url('<?=base_url('frond/');?>img/doctors-bg.jpg') center center;">
    <div class="container">
      <h1>Doctors</h1>
      <p>Caption</p>
    </div>
</section><!-- End Hero -->

 <section id="team" class="team">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        <div class="section-title">
          <h2>doctors</h2>
          <p>Check our Doctors</p>
        </div>

        <div class="row">
          <?php
          if(!empty($doctors))
          {
            foreach($doctors as $d_key)
            {
            ?>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="<?=img_vlid('doctors',$d_key->path);?>" class="img-fluid" alt="">
                <div class="social">
                  <a href="<?=$d_key->twitter;?>"><i class="bi bi-twitter"></i></a>
                  <a href="<?=$d_key->facebook;?>"><i class="bi bi-facebook"></i></a>
                  <!-- <a href="<?=$d_key->linkedin;?>"><i class="bi bi-instagram"></i></a> -->
                  <a href="<?=$d_key->linkedin;?>"><i class="bi bi-linkedin"></i></a>
                 
                </div>
              </div>
              <div class="member-info">
                 <h4><?=$d_key->name;?> </h4>
                <span><?=$d_key->short_description;?></span>
              </div>
            </div>
          </div>
          <?php 
            }
          } ?>
        </div>

      </div>
    </section>
  
    <!-- Template Main JS File -->
     <style type="text/css">.navbar a:hover,
.navbar .doctor,
.navbar .doctor:focus,
.navbar li:hover>a {
  color: #1977cc;
  border-color: #1977cc;
}
</style>
    
