<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url('style/backend/assets/js/')?>jquery.validate.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>

  $(function() {

    $("#contactForm").validate({
    
    rules: {
        name: {
        required: true,
        },
       
    },
    messages: {
        
    },
        submitHandler: function(form,e) {
            e.preventDefault();
            
            $('#contactForm .btn-primary' ).html('<i class="spinner-grow spinner-grow-sm"></i>Loading...');
            $('#contactForm .btn-primary' ).prop('disabled', true);
            formData =  new FormData(document.getElementById('contactForm'));

            $.ajax({
                method : form.method,
                url    : form.action,
                data   : formData,
                contentType: false,
                processData: false,
                dataType : 'json',
                success:function(res) {
                    console.log(res);
                    if(res.status == 200) {
                        swal("Good job!", res.msg, "success");
                        setTimeout(function() {
                            $('#contactForm .btn-primary' ).html('Submit');
                            $('#contactForm .btn-primary' ).prop('disabled', false);
                            $('#contactForm').trigger("reset");
                        }, 200);

                    }else{
                        $('#contactForm .btn-primary' ).html('Submit');
                        swal('oops',res.msg,'error');
                    }
                }

            })
            return false;
        }
    });
});


</script>