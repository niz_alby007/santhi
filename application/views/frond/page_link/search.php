<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h4> </h4>
    </div>
    <div class="modal-body">
     <form action="" method="post" class="php-email-form">
        <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="100" id="contact">
            <h5> Area (sqft)</h5>
            <div class="form-group col-lg-6">
                    <input type="text" name="min" value="<?=($minarea ==$minarea ? $minarea :'');?> " class="form-control input-min" id="min-sq" placeholder="Min" required>
                </div>
                
                <div class="form-group col-lg-6">
                    <input type="text" name="max" value="<?=($maxarea ==$maxarea ? $maxarea :'');?> " class="form-control input-max" id="max-sq" placeholder="Max" required>
                </div>
                <div class="row mt-2">
                    <a id="minMaxCheck" href="javascript:void(0)" class="btn btn-secondary btn-sm text-white">Check</a>
                </div>
                
            <div class="form-group col-lg-12 mt-3">  
               <h5>Bedrooms</h5> 
                <div class="radio bedRooms">
                    <?php if(!empty($bed)){ echo '<input  type="hidden" id="any" name="bed" checked value="'.$bed.'" >'; }?>
                	<input label="Any" type="radio" id="any" name="bed" value="" <?= (!empty($bed) ? 'checked':0);?> >
                    <input label="Studio" type="radio" id="Studio" name="bed" value="Studio">
                	<input label="1" type="radio" id="1" name="bed" value="1">
                    <input label="2" type="radio" id="2" name="bed" value="2">
                    <input label="3" type="radio" id="3" name="bed" value="3">
                    <input label="4" type="radio" id="4" name="bed" value="4">
                    <input label="5" type="radio" id="5" name="bed" value="5">
                    <input label="6" type="radio" id="6" name="bed" value="6">
                    <input label="7" type="radio" id="7" name="bed" value="7">
                    <input label="8+" type="radio" id="8" name="bed" value="8">
                   
                </div>
            </div>
                
            <div class="form-group col-lg-12 mt-3">
                <h5>Bath</h5> 
                <div class="radio bathRooms">
                <?php if(!empty($bath)){ echo '<input  type="hidden"" name="bath" checked value="'.$bath.'" >'; }?>
                    <input label="Any" type="radio" id="any" name="bath" value="" >
                	<input label="1"   type="radio" id="1" name="bath" value="1">
                    <input label="2"   type="radio" id="2" name="bath" value="2">
                    <input label="3"   type="radio" id="3" name="bath" value="3">
                    <input label="4"   type="radio" id="1" name="bath" value="4">
                    <input label="5+"  type="radio" id="2" name="bath" value="5+">
    
                </div>
            </div>
            <div class="form-group col-lg-12 mt-3">
                <div class="radio furniShedStatus">
                <?php if(!empty($furnished)){ echo '<input  type="hidden" name="furnished_ststus" checked value="'.$furnished.'" >'; }?>
                    <input label="Any" type="radio" id="furnishedany" name="furnished_ststus" value="" >             
                	<input label="Furnished" type="radio" id="furnished" name="furnished_ststus" value="furnished" >
                    <input label="Semi Furnished" type="radio" id="semi_furnished" name="furnished_ststus" value="semi furnished">
                    <input label="unfurnished" type="radio" id="unfurnished" name="furnished_ststus" value="unfurnished">
                </div>
            </div>
                <!--<div class="text-center py-2"><button type="submit">Apply</button></div>-->
                </div>
                
                
              </form>
              
              
            
       </div>
  
  </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
<script src="<?=base_url('frond/js/search.js')?>"></script>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtt");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
<style type="text/css">

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 50px;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  /*background-color: rgb(0,0,0); /* Fallback color */
  /*background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: 0 auto;
  padding: 0;
  border: 1px solid #888;
  width: 30%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}
@media (max-width: 992px) {
	.modal-content {width:90%;}


}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color:var(--color-primary);
  color: white;
}

.modal-body {padding: 10px 16px; }

.modal-footer {
  padding: 2px 16px;
  background-color: var(--color-primary);
  color: white;
}


/* radio button--*/




.radio {
	
	padding:1px;
	border-radius: 3px;
		
	/*background: #454857;
	box-shadow: inset 0 0 0 3px rgba(35, 33, 45, 0.3),
		0 0 0 3px rgba(185, 185, 185, 0.3);*/
	position: relative;
}

.radio input {
	width: auto;
	height: 100%;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	outline: none;
	cursor: pointer;
	border-radius: 2px;
	padding: 6px 13px;
	background: #454857;
	color: #bdbdbdbd;
	font-size: 16px;
	font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
		"Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
		"Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
	transition: all 100ms linear;
	margin-bottom:5px;
}

.radio input:checked {
	/*background-image: linear-gradient(180deg, #95d891, #74bbad);*/
	background-image: linear-gradient(180deg, #D0B932, #D0B932);
	color: #000;
	box-shadow: 0 1px 1px #0000002e;
	text-shadow: 0 1px 0px #79485f7a;
}

.radio input:before {
	content: attr(label);
	display: inline-block;
	text-align: center;
	width: 100%;
}


</style>
<script>
     $(document).ready(function () {
            setAutoComplete();
        });

        <?php if(!empty($bed))
        { ?>
            $(".bedRooms input[value='"+<?=$bed?>+"']").prop("checked", true);
        <?php 
        }?>
        <?php if(!empty($bath))
        { ?>
            $(".bathRooms input[value='"+<?=$bath?>+"']").prop("checked", true);
        <?php 
        }?>
        <?php if(!empty($furnished))
        { ?>
            $(".furniShedStatus input[value='<?=$furnished?>']").prop("checked", true);
        <?php 
        }?>

</script>