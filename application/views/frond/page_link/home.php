
<script  src="<?=base_url('frond/js/carousel.min.js');?>"></script>
<script  src="<?=base_url('frond/js/custom-carousel.js');?>"></script>


<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtt");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
<style type="text/css">

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 50px;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  /*background-color: rgb(0,0,0); /* Fallback color */
  /*background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  z-index: 8
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: 0 auto;
  padding: 0;
  border: 1px solid #888;
  width: 30%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}
@media (max-width: 992px) {
	.modal-content {width:90%;}


}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color:var(--color-primary);
  color: white;
}

.modal-body {padding: 10px 16px; }

.modal-footer {
  padding: 2px 16px;
  background-color: var(--color-primary);
  color: white;
}


/* radio button--*/




.radio {
	
	padding:1px;
	border-radius: 3px;
		
	/*background: #454857;
	box-shadow: inset 0 0 0 3px rgba(35, 33, 45, 0.3),
		0 0 0 3px rgba(185, 185, 185, 0.3);*/
	position: relative;
}

.radio input {
	width: auto!important;
	height: 100%;
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	outline: none;
	cursor: pointer;
	border-radius: 2px!important;
	padding: 6px 13px!important;
	background: #454857;
	color: #bdbdbdbd;
	font-size: 16px;
	font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
		"Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji",
		"Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
	transition: all 100ms linear;
	margin-bottom:5px;
}

.radio input:checked {
	/*background-image: linear-gradient(180deg, #95d891, #74bbad);*/
	background-image: linear-gradient(180deg, #D0B932, #D0B932);
	color: #000;
	box-shadow: 0 1px 1px #0000002e;
	text-shadow: 0 1px 0px #79485f7a;
}

.radio input:before {
	content: attr(label);
	display: inline-block;
	text-align: center;
	width: 100%;
}


</style>

<script>
        $(document).ready(function () {
            setHomeSliderHeight();
            setAutoComplete();
        });

    </script>