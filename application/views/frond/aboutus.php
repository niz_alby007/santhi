
  <section id="hero" class="sub-head d-flex align-items-center" style="background: url('<?=base_url('frond/');?>img/about-banner.jpg') top center ;">
    <div class="container">
      <h1><?=$pageTitle;?></h1>
      <p>Caption</p>
     
    </div>
</section><!-- End Hero -->


 <!-- ======= About Section ======= -->
    <section id="" class="about">
      <div class="container">
      
       <div class="section-title">
          
          <p></p>
        </div>

      

        <div class="row">
          <div class="col-xl-5 col-lg-5 video-box d-flex justify-content-center align-items-stretch position-relative" style="background-image: url('<?=img_vlid('aboutus',get_appdata('about_greeting_file'));?>');">
           
          </div>

          <div class="col-xl-7 col-lg-7 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-3 px-lg-5">
            <h3>Greeting</h3>
           <?=get_appdata('abt_greeting')?>
            
            

            <div class="icon-box">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Lorem Ipsum</a></h4>
            
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Nemo Enim</a></h4>
            
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-atom"></i></div>
              <h4 class="title"><a href="">Dine Pad</a></h4>
             
            </div>
            
        
          
           
          
        </div>

      </div>
      
      
    </section><!-- End About Section -->
    
    
    
    
    <!--  vision-->
    <section id="" class="about pt-5">
      <div class="container">
      
       <div class="section-title">
          
          <p></p>
        </div>

      

        <div class="row">
        
          <div class="col-xl-7 col-lg-7 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-3 px-lg-5">
            <h3>Vision</h3>
           <?=get_appdata('abt_vission')?>
            
            
             <div class="icon-box">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">one</a></h4>
            
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">two</a></h4>
            
            </div>

            <div class="icon-box">
              <div class="icon"><i class="bx bx-atom"></i></div>
              <h4 class="title"><a href="">three</a></h4>
             
            </div>
            
        
            
            </div>
            
              <div class="col-xl-5 col-lg-5 video-box d-flex justify-content-center align-items-stretch position-relative"  style="background-image: url('<?=img_vlid('aboutus',get_appdata('about_vission_file'));?>')">
           
          </div>

        </div>

      </div>
      
      
    </section><!-- End vision Section -->
    
    
    
     <!-- ======= mission Section ======= -->
    <section id="" class="about pt-5">
      <div class="container">
      
       <div class="section-title">
          
          <p></p>
        </div>

      

        <div class="row">
          <div class="col-xl-5 col-lg-5 video-box d-flex justify-content-center align-items-stretch position-relative" style="background: url('<?=img_vlid('aboutus',get_appdata('about_mission_file'));?>') center center no-repeat;">
           
          </div>

          <div class="col-xl-7 col-lg-7 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-3 px-lg-5">
            <h3>Mission</h3>
           <?=get_appdata('abt_mission')?>
          
          
           
          
        </div>

      </div>
      
      
    </section><!-- End mission Section -->
    
  
  
  
  
  
   <section id="team" class="team">
      <div class="container aos-init aos-animate" data-aos="fade-up">

        <div class="section-title">
          <h2>Director's Board</h2>
          <p>Check our Board</p>
        </div>

         <div class="row">
          <?php
          if(!empty($doctors))
          {
            foreach($doctors as $d_key)
            {
            ?>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="<?=img_vlid('doctors',$d_key->path);?>" class="img-fluid" alt="">
                <div class="social">
                  <a href="<?=$d_key->twitter;?>"><i class="bi bi-twitter"></i></a>
                  <a href="<?=$d_key->facebook;?>"><i class="bi bi-facebook"></i></a>
                  <!-- <a href="<?=$d_key->linkedin;?>"><i class="bi bi-instagram"></i></a> -->
                  <a href="<?=$d_key->linkedin;?>"><i class="bi bi-linkedin"></i></a>
                 
                </div>
              </div>
              <div class="member-info">
                 <h4><?=$d_key->name;?> </h4>
                <span><?=$d_key->short_description;?></span>
              </div>
            </div>
          </div>
          <?php 
            }
          } ?>
        </div>

      </div>
    </section>
  
    
    
    
    
    
    
    <!-- Template Main JS File -->
<style type="text/css">
	.navbar a:hover,
.navbar .about,
.navbar .about:focus,
.navbar li:hover>a {
  color: #1977cc;
  border-color: #1977cc;
}
 </style>
<!---->
  
  
