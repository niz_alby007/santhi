
 <!-- ======= Hero Section ======= -->
  <section id="hero" class="sub-head d-flex align-items-center" style="background-image: url('<?=img_vlid('department',$department->path);?>');     background-size: cover!important;
;">
    <div class="container">
      <h1><?=$pageTitle;?></h1>
      <p>Caption</p>
     
    </div>
</section><!-- End Hero -->
<section id="" class="doctors">
  <div class="section-title">
    <h2><?=$pageTitle;?></h2>
  </div>

  <div class="container py-5">
    <div class="row">
      <div class="col-lg-6" style="max-height:400px; overflow:auto;">
        <?=(!empty($department->description) ? $department->description: '') ;?>
      </div>         
  <?php 
      if(!empty($images))
      { ?>
  <div class="col-lg-6 order-1 order-lg-2 text-center" data-aos="fade-up" data-aos-delay="200"  style="max-height:400px;">
  <div class="slides-1 swiper">
    <div class="swiper-wrapper">
      <?php
        foreach($images as $img)
        { ?>
      <div class="swiper-slide ">
        <div class="item fit-object" >
          <img src="<?=img_vlid('innerdepartment',$img->images);?>" alt="" class="img-fluid" >
        </div>
      </div><!-- End slide item -->
      <?php 
      } ?>
  

    </div>

    <div class="swiper-pagination"></div>
    <div class="swiper-button-prev" style="font-size:12px; width:10px;"></div>
    <div class="swiper-button-next"></div>       
  </div>
</div>
 <?php } ?>        
</div></div>
</section>

<!-- =======doctors Section ======= -->
    <section id="dtp" class="dpt">
      <div class="container pb-5">

        <div class="section-title">
          <h2>Doctors</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">

           <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">
            <?php
            if(!empty($doctors))
            {
              foreach($doctors as $d_key)
              {
                ?>
            <div class="swiper-slide">
            
            <div class="member d-flex align-items-start">
              <div class="row">
              <div class="pic col-lg-5"><img src="<?=img_vlid('doctors',$d_key->path);?>" class="img-fluid" alt=""></div>
              <div class="member-info col-lg-7">
                <h4><?=$d_key->name;?></h4>
                <span><?=$d_key->degree_for_surgeons;?></span>
                <p><?=$d_key->short_description;?></p>
                <div class="social">
                  <a href="<?=$d_key->twitter;?>"><i class="ri-twitter-fill"></i></a>
                  <a href="<?=$d_key->facebook;?>"><i class="ri-facebook-fill"></i></a>
                  <a href="https://wa.me/<?=$d_key->contact_number;?>"><i class="ri-whatsapp-fill"></i></a>
                  <!-- <a href="" title="More Details"> <i class="bx bx-chevron-right"></i> </a> -->
              
                </div>
              </div>
              
              </div><!--row-->
            </div>
          </div>
        <?php }
        } ?>

        
        </div>  
        <div class="swiper-pagination" ></div>
          <!-- <div class="swiper-button-prev" style="font-size:12px; width:10px;"></div>
            <div class="swiper-button-next"></div> 
-->
      </div>
      
   
      
      </div></div>




  </section><!-- End department Section -->
  <section id="team" class="team">
    <div class="container aos-init aos-animate " data-aos="fade-up" >
      <div class="doctors-slider swiper">
        <div class="section-title">
          <h2>Doctors</h2>
          <p>Check our Doctors</p>
        </div>
    
        <div class="swiper-wrapper align-items-center services"> 
        <?php
        if(!empty($doctors))
        {
          foreach($doctors as $d_key)
          {
          ?>
          <div class="swiper-slide d-flex align-items-stretch ">
            <div class="member aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="<?=img_vlid('doctors',$d_key->path);?>" class="img-fluid" alt="">
                <div class="social">
                  <a href="<?=$d_key->twitter;?>"><i class="bi bi-twitter"></i></a>
                  <a href="<?=$d_key->facebook;?>"><i class="bi bi-facebook"></i></a>
                  <!-- <a href="<?=$d_key->linkedin;?>"><i class="bi bi-instagram"></i></a> -->
                  <a href="<?=$d_key->linkedin;?>"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4><?=$d_key->name;?> </h4>
                <span><?=$d_key->short_description;?></span>
              </div>
            </div>
          </div>
          <?php
            }
          }?>

        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>

</section>
  














 <!-- ======= facility ======= -->
      <section id="services" class="services">
      <div class="container" data-aos="zoom-in">

        <div class="clients-slider swiper">
        
        
         <div class="section-title">
          <h2>Facilties</h2>
          <p></p>
        </div>
        <div class="clients-slider swiper">
          <div class="swiper-wrapper align-items-center  mb-5 "> 
          <?php 
            if(!empty($facilities))
            {
              foreach($facilities as $f_key)
              {
                ?>

                <div class="swiper-slide  d-flex align-items-stretch mt-4 mt-md-0 p-3">
                <div class="icon-box">
                   <div class="icon col-lg-12" style="background-image: url('<?=img_vlid('facilities',$f_key->path)?>'"></div>
                  <h4><a href=""><?=$f_key->title;?></a></h4>
                  <p><?=$f_key->short_description;?></p>
                   <a href="<?=base_url('facilities-view/').$f_key->slug;?>" class="appointment-btn"><i class="bx bx-chevron-right"></i></a>
                </div>
              </div>
              <?php 
                }
              }
              ?>
           
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
      
      
     
    </section><!-- End Clients Section -->
    
    
    

 <!-- Template Main JS File -->
  <style type="text/css">.navbar a:hover,
.navbar .department,
.navbar .department:focus,
.navbar li:hover>a {
  color: #1977cc;
  border-color: #1977cc;
}
</style>
 
