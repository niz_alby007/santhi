<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> <?=$pageTitle;?></title>
    <?php
    if ( isset($this->data['assets']['meta']) && !empty($this->data['assets']['meta']) ) {
        foreach ($this->data['assets']['meta'] as $_h_asset) {
            $this->load->view($_h_asset);
        }
    }else{ ?>
        <meta content="" name="keywords">
        <meta content="" name="description">
    <?php }
    ?>

    

    <?php $this->load->view('frond/inc/script.php') ;?>
 <?php
    if ( isset($this->data['assets']['header']) && !empty($this->data['assets']['header']) ) {
        foreach ($this->data['assets']['header'] as $_h_asset) {
            $this->load->view($_h_asset);
        }
    }
    ?>
<?php $this->load->view('frond/inc/header') ;?> 
<?php $this->load->view('frond/'.$template);?> 
<?php $this->load->view('frond/inc/footer') ;?>           
<?php $this->load->view('frond/inc/footer_link') ;?>
 <?php
    if ( isset($this->data['assets']['footer']) && !empty($this->data['assets']['footer']) ) {
        foreach ($this->data['assets']['footer'] as $_f_asset) {
            $this->load->view($_f_asset);
        }
    }
  ?>
</body>

</html>