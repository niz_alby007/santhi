</head>
<body>
 <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope"></i> <a href="mailto:<?=get_appdata('site_email')?>"><?=get_appdata('site_email')?></a> &nbsp; 
        <a href="tel:<?=get_appdata('contact_number')?>"> <i class="bi bi-phone"></i><?=get_appdata('contact_number')?></a>
      </div>
      <div class="d-none d-lg-flex social-links align-items-center">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
    </div>
  </div>
  <!---------------------------header----------->
 <?php
 $headerDepartments = $this->common->get_department('','');
 ?>
 <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="<?=base_url();?>">Santhi</a></h1>
     <a href="index.html" class="logo me-auto"><img src="<?=base_url('frond/');?>assets/img/logo.png" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link  active" href="<?=base_url();?>">Home</a></li>
          <li><a class="nav-link  about" href="<?=base_url('aboutus');?>">About Us</a></li>
         
          <li><a class="nav-link facility" href="<?=base_url('facilities');?>">Facilities</a></li>
          
          <li><a class="nav-link doctor" href="<?=base_url('doctors');?>">Doctors</a></li>
          <li class="dropdown "><a href="<?=base_url('department');?>" class="department"><span>Departments</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <?php
              if(!empty($headerDepartments))
              {
                foreach ($headerDepartments as $h_department) { ?>
                 <li><a href="<?=base_url('departments-view/').$h_department->slug;?>"><?=$h_department->category;?></a></li>
                 <?php 
                }
              } ?>
            </ul>
          </li>
          
           <li><a class="nav-link scrollto gallery" href="<?=base_url('gallery');?>">Gallery</a></li>
          <li><a class="nav-link scrollto contact" href="<?=base_url('contactus');?>">Contact</a></li>
          
           <li><a class="nav-link faq" href="<?=base_url('faq');?>">FAQ</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      <a href="tel:9747470679" class="appointment-btn scrollto"><span class="d-none d-md-inline">Make an</span> Appointment</a>

    </div>
  </header>