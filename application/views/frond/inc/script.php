  <!-- Favicons -->
  <link href="<?=img_vlid('festival_img',get_appdata('festival_img'))?>" rel="icon">
  <link href="<?=img_vlid('festival_img',get_appdata('festival_img'))?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
 <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">-->

  <!-- Vendor CSS Files -->
  <link href="<?=base_url('frond/');?>plugin/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=base_url('frond/');?>plugin/swiper/swiper-bundle.min.css" rel="stylesheet">
 <!-- Template Main CSS File -->
  <link href="<?=base_url('frond/');?>css/style.css" rel="stylesheet">