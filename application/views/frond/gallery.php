
 <!-- ======= Hero Section ======= -->
  <section id="hero" class="sub-head d-flex align-items-center" style="background: url('<?=base_url('frond/');?>img/doctors-bg.jpg') center center;">
    <div class="container">
      <h1>Doctors</h1>
      <p>Caption</p>
     
    </div>
</section><!-- End Hero -->



 <!-- ======= Gallery Section ======= -->
  <section id="gallery" class="gallery">
      <div class="container">

        <div class="section-title">
          <h2>Gallery</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
      </div>

      <div class="container">
        <div class="row g-0">

          <?php 
          if(!empty($gallery))
          {
            foreach($gallery as $gall_key)
            { ?>
              <div class="col-lg-3 col-md-4">
            <div class="gallery-item">
              <a href="<?=img_vlid('gallery',$gall_key->path)?>" class="galelry-lightbox">
                <img src="<?=img_vlid('gallery',$gall_key->path)?>" alt="<?=$gall_key->title;?>" class="img-fluid">
              </a>
            </div>
          </div>

          <?php  }
          } ?>

         

        </div>

      </div>
    </section><!-- End Gallery Section -->

  
    <!-- Template Main JS File -->
     <style type="text/css">.navbar a:hover,
.navbar .gallery,
.navbar .gallery:focus,
.navbar li:hover>a {
  color: #1977cc;
  border-color: #1977cc;
}
</style>
    
