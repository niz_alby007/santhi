<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');

		
   }
	
	public function index()
	{
		$pageTitle= "Doctors";
		$doctors = $this->common->get_alldata('*','doctors',array('status' =>1 ));
		$this->addData(compact('pageTitle','doctors'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("doctors"); 
	}
}