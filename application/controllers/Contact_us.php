<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Contact_us extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');
		
   }
	
	public function index()
	{
		$pageTitle= "Gallery";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	'frond/page_link/ft_contact',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("contactus"); 
	}
	function inquiry(){
		$valid['success'] = array('success'=>true,'status'=>400,'msg'=> array());
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('department','Department','required');
		$this->form_validation->set_rules('message','message','required');
		

		
		if($this->form_validation->run() == TRUE) {

			require 'mailer/PHPMailer/src/Exception.php';
			require 'mailer/PHPMailer/src/PHPMailer.php';
			require 'mailer/PHPMailer/src/SMTP.php'; 
		    $messagess   =
		    '<table><tr><td>Name:</td><td>'.$this->input->post('name').'</td></tr>
		    <tr><td>Email:</td><td>'.$this->input->post('email').'</td></tr>
		    <tr><td>Phone:</td><td>'.$this->input->post('full').'</td></tr>
		    <tr><td>Message:</td><td>'.$this->input->post('message').'</td></tr>
		    <tr><td>Page:</td><td>'.$this->input->post('current').'</td></tr>'; 
		    $mail = new PHPMailer(true);
            //$mail->SMTPDebug = true;
            $mail->isSMTP();             // Send using SMTP
            $mail->Host       = 'orielteq.com';//'smtp.gmail.com';                        // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
           // $mail->Username   = 'arpisdmm@gmail.com';                     // SMTP username
            //$mail->Password   = 'prmkhglxfmzpjsnd';                               // SMTP password
            $mail->Username   = 'info@orielteq.com';                     // SMTP username
            $mail->Password   = 'orielteq12120!';                               // SMTP password
           // $mail->SMTPSecure = 'tls'; //'PHPMailer::ENCRYPTION_STARTTLS';         // Enable TLS       encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->SMTPSecure = 'tls'; 
            $mail->Port     = 587;
            
            //Recipients
            $mail->setFrom('info@orielteq.com', get_appdata('title'));
            $mail->addReplyTo($this->input->post('email'));
           //$mail->addAddress(get_appdata('site_email'));   
            $mail->addAddress('info@orielteq.com');  
            $mail->isHTML(true);                                 // Set email format to HTML
            $mail->Subject = "Lead from Arpis Website";
            $mail->Body    = $messagess;
            
            if($mail->send()){
                  $valid['status'] =200;
                  // if(!empty($this->input->post('interest')))
                  // {
                      $cmsg = nl2br("Thank you for submitting your interest. We are delighted to inform you that we have assigned our Department expert to contact you to provide a free consultation for your enquiry."."\r\n"." Meanwhile, if you wish to get in touch with us, please don’t hesitate to contact us at <a href='tel:".get_appdata('contact_number')."'>".get_appdata('contact_number')."</a>.");
                  // }else{
                  //     $cmsg ="Thank you for your interest! We will be in touch with you shortly regarding your inquiry. Have a nice day.!";
                  // }
                $confirm = '
							<div style="border:1px #000 solid;padding10px; width:80%; margin:0 auto;">
										<h4>Dear '.$this->input->post('name').',</h4>
											<p style="margin:15px; line-height: 18px; letter-spacing: 1px; font-weight: 600; text-align: left;font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;">
												'.$cmsg.'			
											</p>
								</div>
							</div>';
							

		
                $mailr = new PHPMailer(true);
                //$mail->SMTPDebug = true;
                $mailr->isSMTP();             // Send using SMTP
                $mailr->Host       = 'orielteq.com';//'smtp.gmail.com';                        // Set the SMTP server to send through
                $mailr->SMTPAuth   = true;                                   // Enable SMTP authentication
                //$mailr->Username   = 'arpisdmm@gmail.com';                     // SMTP username
                //$mailr->Password   = 'prmkhglxfmzpjsnd';                               // SMTP password
                $mailr->Username   = 'info@orielteq.com';                     // SMTP username
                $mailr->Password   = 'orielteq12120!';                               // SMTP password
               // $mail->SMTPSecure = 'tls'; //'PHPMailer::ENCRYPTION_STARTTLS';         // Enable TLS       encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                $mailr->SMTPSecure = 'tls'; 
                $mailr->Port     = 587;
                
                $mailr->setFrom('info@orielteq.com', get_appdata('title'));
                //$mailr->addReplyTo(get_appdata('site_email
                $mailr->addReplyTo('info@orielteq.com');
                $mailr->addAddress($this->input->post('email'));   
                $mailr->isHTML(true);                                 // Set email format to HTML
                $mailr->Subject = "Confirmation email from Arpis Real Estate";
                $mailr->Body    = $confirm;
                $mailr->send();
                            
                            
                $valid['status'] = 200;
                $valid['msg'] = " Thank you for your interest. Our consultant  will contact you shortly";
                $this->session->set_flashdata('msg',$cmsg);
                //redirect('success');
            }else{
                $this->session->set_flashdata('fronterrormsg','Something went wrong please try later');
                $valid['msg'] = "Something went wrong please try later";
                //redirect('success');
            }
		}
		else{
		    $this->session->set_flashdata('fronterrormsg','Please Fill out All required Fields');
		    $valid['msg'] = "Please Fill out All required Fields";
		    //redirect('success');
		}
		echo json_encode($valid);
		//redirect('success');
	}
}