<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');
	
   }
	
	public function index()
	{
		$pageTitle= "Facilities";
		$facilities = $this->common->get_alldata('*','facilities',array('status' =>1 ));
		$this->addData(compact('pageTitle','facilities'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("facilities");  
	}

	public function view($slug=FALSE)
	{  	
		if(!empty($slug))
		{
			$facilitiesview = $this->common->get_row('facilities','*',array('status' =>1,'slug'=>$slug ));
			if(!empty($facilitiesview))
			{
				$pageTitle = $facilitiesview->title;
				$data      = $facilitiesview;
				$images    = $this->common->get_alldata('images','department_images',array('department_id' =>$facilitiesview->id ));
			}else
			{
				$pageTitle = "Facilities";
				$data      = '';
				$images    = '';
			}
		}else{
			$pageTitle = "Facilities";
			$data      = '';
			$images    = '';
		}

		
		$this->addData(compact('pageTitle','data','images'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("facilities-view"); 
	}
}