<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use vender\Google\Payment\Request\PaymentDataRequest;

class Payment extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');
   }
   	public function createPaymentDataRequest() {
	    $paymentDataRequest = new PaymentDataRequest();
	    $paymentDataRequest->apiVersion = 2;
	    $paymentDataRequest->apiVersionMinor = 0;
	    $paymentDataRequest->merchantInfo = new MerchantInfo();
	    $paymentDataRequest->merchantInfo->merchantId = "YOUR_MERCHANT_ID";
	    $paymentDataRequest->merchantInfo->merchantName = "YOUR_MERCHANT_NAME";
	    $paymentDataRequest->transactionInfo = new TransactionInfo();
	    $paymentDataRequest->transactionInfo->totalPriceStatus = "FINAL";
	    $paymentDataRequest->transactionInfo->totalPrice = "YOUR_TOTAL_PRICE";
	    $paymentDataRequest->transactionInfo->currencyCode = "YOUR_CURRENCY_CODE";
	    return $paymentDataRequest;
	}
}
	