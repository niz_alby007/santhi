<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departments extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');
		  $this->load->model('frond/department_model');
		  $this->load->model('doctors_model');
		
   }
	
	public function index()
	{
		$pageTitle= "Department";
		$departments = $this->common->get_department();
		$this->addData(compact('pageTitle','departments'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("departments"); 
	}
	function view($slug = FALSE)
	{	
		
		if($slug)
		{
			$department = $this->department_model->get_department_finder(array('t1.status'=>1,'slug'=>$slug));

			$pageTitle	= $department->category;
			if(!empty($department->id))
			{
				$images = $this->common->get_alldata('images','department_images',array('department_id' =>$department->id ));
				//echo $this->db->last_query();
			}
		    $a =  unserialize($department->doctors);
			$doctors  	= $this->doctors_model->doctors('doctors','*','',$a,'');
			//echo $this->db->last_query();exit();

		}else{
			$pageTitle  = "Department ";
			$department =  '';
			$doctors    = $doctors;
			$images     = '';
		}
		$facilities = $this->common->get_alldata('*','facilities',array('status' =>1 ));
		$this->addData(compact('pageTitle','department','doctors','images','facilities'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("departments-view");
	}
}