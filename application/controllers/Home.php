<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');
		  $this->load->model('doctors_model');
		
   }
	
	public function index()
	{	
	
		$pageTitle= "Home";
		// $team      = $this->common->get_alldata('*','team',array('status'=>1));
		 $slider      = $this->common->get_alldata('*','slider',array('status'=>1));
		 $departments = $this->common->get_department(4,0);
		 $facilities = $this->common->get_alldata('*','facilities',array('status' =>1 ));
		 $testimonials = $this->common->get_alldata('*','testimonial',array('status' =>1 ));
		
	 
		$this->addData(compact('pageTitle','slider','departments','facilities','testimonials'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("home"); 
	}

}
