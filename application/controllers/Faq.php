<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');

		
   }
	
	public function index()
	{
		$pageTitle= "Faq";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("faq"); 
	}
}