<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->library('form_validation');
		
   }
   function delete()
   {	
   		$valid['success'] = array('status'=>400,'msg'=>array());
   		$id      = decryptor($this->input->post('id'));
   		$path    = $this->input->post('path');
   		$folder  = $this->input->post('folder');
   		$table   = $this->input->post('tbl');
   		$tablId  = $this->input->post('tblid');
       
   		if(!empty($id)) {
   			$this->common->deleteData($table,array($tablId=>$id),$folder,$path);
   			$valid['msg']    = 'this Item was deleted'; 
   			$valid['status'] = 200;
   		}else{
			$valid['msg']    = 'Opps . Please Try Later'; 
		}
   		echo json_encode($valid);
   }
}