<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->model('category_model');
		$this->load->library('form_validation');
		
   }

	public function index()
	{	
		$pageTitle = "Ctegory List";
		//$district = $this->common->get_alldata('*','location',array('status' => 1),'id ASC');
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_category'
                ]
            ]);
		$this->render("category/index");
	}

    public function list(){
		$data 	  = array();
		$postData = $this->input->post();
		$data     = $this->category_model->categoryList($postData);
     	echo json_encode($data);
	}
    	
	public function create($id = False)
	{	
		if(!empty($id)){
			$id = decryptor($id);
			$pageTitle = "Edit Category";
			$editdata  = $this->common->get_row('categories','*',array('category_id'=>$id));
		}else{
			$editdata  = '';
			$pageTitle = "Create Category";
		}
		$this->addData(compact('pageTitle','editdata'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_category'
                ]
            ]);
		$this->render("category/create");
	}

    public function submit()
	{
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('category','Category','required');
		$id = $this->input->post('id');
		if($this->form_validation->run() == TRUE) {

			$data = array(
				'category'  => $this->input->post('category'),
				'status'    => 1
			);
			if(!empty($id)) {
				$id = decryptor($id);
				$data['updated_at'] = date('Y-m-d,H:i:s');
				//$data['updated_by'] = $this->user_id;
				$this->common->update_data('categories',$data,array('category_id' => $id));
				$valid['msg']  = 'Edit Successfully Completed';
				$valid['status'] = 200;
			}else{
				$data['created_at'] = date('Y-m-d,H:i:s');
				$this->common->insertData('categories',$data);
				$valid['msg']  = 'Request Successfully Completed';
			}
			$valid['status'] = 200;
		}else{
			$valid['msg'] = "Please Fill out all required Fields";
		}
		echo json_encode($valid);
	}


}