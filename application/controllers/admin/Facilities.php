<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->model('facilities_model');
		$this->load->library('form_validation');
		$this->load->model('Image_uploading_model');
		
   }

	public function index()
	{	
		$pageTitle = "Developers ";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_facilities'
                ]
            ]);
		$this->render("facilities/index");
	}
	public function list(){
		$data 	  = array();
		$postData = $this->input->post();
		$data     = $this->facilities_model->developersList($postData);
     	echo json_encode($data);

	}

	public function create($id=false) {
		if(!empty($id)){
			$id = decryptor($id);
			$pageTitle = "Edit Facilities";
			$editdata  = $this->common->get_row('facilities','*',array('id'=>$id));
			$department_images = $this->common->get_alldata('*','department_images',array('department_id'=>$id,'status'=>1));
		}else{
			$editdata  = '';
			$pageTitle = "Create Facilities";
			$department_images = '';
		}
		$this->addData(compact('pageTitle','editdata','department_images'));
		$this->addAssets([
               'header' =>[
                    "admin/head_link/create_proprties"
                ],
                'footer' => [
                	'admin/script/facilities_create',
                    //'admin/script/services'
                ]
            ]);
		$this->render("facilities/create");
	}
	function submit()
    {
        $valid['success'] = array('success'=>false,'msg'=>array(),'status'=>400);

        $this->form_validation->set_rules('page_link', 'Title', 'required');
        $editId  = decryptor($this->input->post('id'));
        $oldPath = $this->input->post('oldpath');
        if($this->form_validation->run() == TRUE)
        {
            $file_data = $this->Image_uploading_model->image_uploader('','facilities');
          
            $data    = array(

                //'property_parent_id'  => $this->input->post('parent_id'),
                'title'         		=> $this->input->post('page_link'),
                'short_description'     => $this->input->post('short_description'),
                'description'           => $this->input->post('doc_description'),
                'status'                => 1,
            );
            $slug = slugify($this->input->post('slug'));

            if(!empty($editId))
            {
                $data['updated_at']  = date('Y-m-d,H:i:s');
                $coverimg     = img_update($file_data,'facilities',$oldPath);
                if(!empty($coverimg)){
                    $data['path'] = $coverimg;
                }
                $dup = $this->common->checkDuplicateData('facilities',array('slug' => $this->input->post('slug'),'id !=' => $editId));
            
                if($dup){ 
                    $data['slug'] = $this->input->post('slug');
                     $this->common->entry_update('facilities', array('id' => $editId),$data);
                    if(!empty($this->input->post('img')))
                    {
                        $this->sub_img($editId);
                    }

                    $valid['status'] = 200;
                    $valid['msg']    = "Done ! Department Successfully Updated";
                }else{
                   $valid['msg']    = "This url already taken Please enter valid one"; 
                }

            }else
            {
                 if(!empty($file_data['file_name']))
                {
                    $imagePath       = $file_data['file_name'];
                    $data['path']    = $imagePath;
                }
                $data['created_at']  = date('Y-m-d,H:i:s');
                $dup = $this->common->checkDuplicateData('facilities',array('slug' => $this->input->post('slug')));
            
                if($dup){
                    $data['slug'] = $this->input->post('slug');
                    $insert       = $this->common->insertData('facilities',$data);
                    //$this->common->insertData('blog',$data);
                    if($insert)
                    {   
                        if(!empty($this->input->post('img')))
                        { 
                            $this->sub_img($insert);
                        }
                        $valid['status'] = 200;
                        $valid['msg']    = "Good Job ! Department Successfully Added";
                    }

                    //$valid['msg']  = 'Request Successfully Completed';
                    //$valid['status'] = 200;
                }else {
                    $valid['msg']  = 'This slug already Taken. Please try much better one';
                }

                
            }
        }else
        {
          $valid['msg'] = "Please Fillout all required Fields !";
        }
      
        echo json_encode($valid);
    }
    function sub_img($id)
    {
        define('PATH_IMG', './uploads/facilitiesslider/');
        create_folder('uploads/facilitiesslider','index','index');
         $data_count = count($this->input->post('img'));
        //if($data_count !=0){
        $image = array();
        for($i = 0; $i < $data_count; $i++)
        {
            $img  = $this->input->post('img')[$i];
            $pos  = strpos($img, ';');
    
            $name = microtime().'.'.explode('/', substr($img, 0, $pos))[1];
            $name = str_replace(' ', '_', $name);
            $img  = str_replace('data:image/png;base64,', '', $img);
            $img  = str_replace('data:image/jpeg;base64,', '', $img);
            $img  = str_replace('data:image/jpg;base64,', '', $img);
            $img  = str_replace(' ', '+', $img);
            $data = base64_decode($img,true);
            if(file_put_contents(PATH_IMG.$name, $data))
            {
                 $uploadImgData[$i]['images']        = $name;
                 $uploadImgData[$i]['department_id'] = $id;
                 $uploadImgData[$i]['created_at']    = date('Y-m-d,H:i:s');
                 $uploadImgData[$i]['status']        = 1;
            }
        }
        if(!empty($uploadImgData))
        {
            $this->common->multiple_insert('department_images',$uploadImgData);
        }
        return true;
    }

    function delete()
    {  
        $valid['success'] = array('status'=>400,'msg'=>array());
        $id      = decryptor($this->input->post('id'));
        if(!empty($id)) {
            $path = base_url('uploads/');
            $basePath = base_url('uploads/');
            
            $offImages = $this->common->get_alldata('*','department_images',array('department_id'=>$id));
           
            if(count($offImages) > 0){
             foreach($offImages as $ofImg){
                if(file_exists('uploads/facilitiesslider/'.$ofImg->images))
                {
                    unlink('uploads/facilitiesslider/'.$ofImg->images);
                }
             }
             $this->common->deleteData('department_images',array('department_id'=>$id),'','');
            }

            $proDtl = $this->common->get_row('facilities','*',array('id'=>$id));
            if(!empty($proDtl)){
                if(file_exists('uploads/facilities/'.$proDtl->path))
                {   
                     unlink('uploads/facilities/'.$proDtl->path);
                }
            }
            $this->common->deleteData('facilities',array('id'=>$id),'','');

            $valid['msg']      = 'This Item was deleted'; 
            $valid['redirect'] = base_url('admin/facilities'); 
            $valid['status']   = 200;
        }else{
            $valid['msg']      = 'Opps . Please Try Later'; 
        }
        
        echo json_encode($valid);
    }
	
}