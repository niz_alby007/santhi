<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->model('doctors_model');
		$this->load->library('form_validation');
		$this->load->model('Image_uploading_model');
		
   	}

   	public function index()
	{	
		$pageTitle = "Doctors ";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_doctors'
                ]
            ]);
		$this->render("doctors/index");
	}
	public function list(){
		$data 	  = array();
		$postData = $this->input->post();
		$data     = $this->doctors_model->doctorsList($postData);
     	echo json_encode($data);

	}
	public function create($id=false) {
		if(!empty($id)){
			$id = decryptor($id);
			$pageTitle = "Edit Doctors";
			$editdata  = $this->common->get_row('doctors','*',array('id'=>$id));
		}else{
			$editdata  = '';
			$pageTitle = "Create Doctors";
		}
		$this->addData(compact('pageTitle','editdata'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_create_doctors'
                ]
            ]);
		$this->render("doctors/create");
	}
	public function submit()
	{
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('designation','Designation','required');
		$id      = $this->input->post('id');
		$oldPath = $this->input->post('path');
		if($this->form_validation->run() == TRUE) {

			$file_data = $this->Image_uploading_model->image_uploader('','doctors');
			$data = array(
				'name'          		=> $this->input->post('name'),
				'degree_for_surgeons'   => $this->input->post('designation'),
				'short_description'     => $this->input->post('short_description'),
				'department'            => $this->input->post('department'),
				'twitter'         		=> $this->input->post('twitter'),
				'contact_number' 		=> $this->input->post('contact_number'),
				'linkedin'     		    => $this->input->post('linkedin'),
				'facebook'     		    => $this->input->post('facebook'),
				'instagram'     		=> $this->input->post('instagram'),
				'status'        		=> 1
			);
			if(!empty($file_data['file_name']))
			{
				$imagePath      = $file_data['file_name'];
				$data['path']   = $imagePath;
			}
				
			if(!empty($id)) {
				$id = decryptor($id);
				$imgvalid = img_update($file_data,'doctors',$oldPath);
				if(!empty($imgvalid)){
					$data['path'] = $imgvalid;
				}
				$data['updated_at'] = date('Y-m-d,H:i:s');
				$data['updated_by'] = $this->user_id;
				$this->common->update_data('doctors',$data,array('id' => $id));
				$valid['msg']  = 'Member Successfully Updated';
				$valid['status'] = 200;
			}else{
				$data['created_at'] = date('Y-m-d,H:i:s');
				$data['created_by'] = $this->user_id;

				$this->common->insertData('doctors',$data);
				$valid['msg']  = 'Successfully Created';
				$valid['status'] = 200;
			}
		}else{

		}
		echo json_encode($valid);
	}
}
	
