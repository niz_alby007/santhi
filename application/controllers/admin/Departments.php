<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departments extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->model('department_model');
		$this->load->library('form_validation');
		$this->load->model('Image_uploading_model');
        $this->perPage = 3;

		
   }
   function index()
   {
    $pageTitle = "Departments";


    $this->addData(compact('pageTitle'));
		$this->addAssets([
                'header' =>[
                    "admin/head_link/h_properties"
                ],
                'footer' => [
                	//'admin/script/ft_properties_create',
                    'admin/script/ft_department'
                ]
            ]);
    $this->render('department/index');
   }

    public function list(){
        $data     = array();
        $postData = $this->input->post();
        $data     = $this->department_model->departmentList($postData);
        echo json_encode($data);

    }
   function create($id= FALSE)
   {
    if (!empty($id))
    {
        $id = decryptor($id);
        $pageTitle = "Edit Department";
        $editdata  = $this->common->get_row('departments','*',array('id'=>$id));
        $department_images = $this->common->get_alldata('*','department_images',array('department_id'=>$id,'status'=>1));
    }else{
        $editdata  = '';
        $pageTitle = "Add Department";
        $amenities = '';
        $department_images = '';
    }
    
    
    $this->addData(compact('pageTitle','editdata','department_images'));
		$this->addAssets([
                'header' =>[
                    "admin/head_link/create_proprties"
                ],
                'footer' => [
                	'admin/script/ft_department_create',
                    //'admin/script/services'
                ]
            ]);
    $this->render('department/create');
   }

   function get_doctors()
   {
    $valid['success'] = array('status'=> 400 ,'msg'=>array());
        $this->form_validation->set_rules('id','Department','required');
        $html = '';
        $id = $this->input->post('id');
        if($this->form_validation->run() == TRUE) {
            if(!empty($id))
            {
                $result =  $this->common->get_alldata('*','doctors',array('department'=>$id,'status'=>1));
                 //$valid['qry'] = $this->db->last_query();
                if(!empty($result))
                {   $html .='<option value=" ">Choose Doctors</option>';
                    foreach($result as $r_key)
                    {
                        $html .='<option  value="'.$r_key->id.'">'.$r_key->name.'</option>';
                    }
                }else
                {
                    $html .='<option value="">No Data Found</option>';
                }
                $valid['msg']    = 'Request Successfully Completed';
                $valid['status'] = 200;
                $valid['result'] = $html;
            }else{
                $valid['result'] ='<option value="">No Data Found</option>';
                $valid['msg'] = "Please choose a valid Department";
            }
            
        }else{
            $valid['result'] ='<option value="">No Data Found</option>';
            $valid['msg'] = "Please choose a valid Department";
        }
        echo json_encode($valid);
   }

    function submit()
    {
        $valid['success'] = array('success'=>false,'msg'=>array(),'status'=>400);

        $this->form_validation->set_rules('department_type', 'Department Type', 'required');
        $editId  = decryptor($this->input->post('id'));
        $oldPath = $this->input->post('oldpath');
        if($this->form_validation->run() == TRUE)
        {
            $file_data = $this->Image_uploading_model->image_uploader('','department');
            if(!empty($this->input->post('doctors'))) {
                $doctors = implode(',',$this->input->post('doctors'));
            }else{
                $doctors = array();
            }
           
            $data    = array(

                //'property_parent_id'  => $this->input->post('parent_id'),
                'department_id'         => $this->input->post('department_type'),
                'doctors'               => serialize($this->input->post('doctors')),
                'contact_number'        => $this->input->post('ph'),
                'short_description'     => $this->input->post('short_description'),
                'description'           => $this->input->post('doc_description'),
                'link'                  => $this->input->post('page_link'),
                'status'                => 1,
            );
            $slug = slugify($this->input->post('slug'));

            if(!empty($editId))
            {
                $data['updated_at']  = date('Y-m-d,H:i:s');
                $coverimg     = img_update($file_data,'department',$oldPath);
                if(!empty($coverimg)){
                    $data['path'] = $coverimg;
                }
                $dup = $this->common->checkDuplicateData('departments',array('slug' => $this->input->post('slug'),'id !=' => $editId));
            
                if($dup){ 
                    $data['slug'] = $this->input->post('slug');
                     $this->common->entry_update('departments', array('id' => $editId),$data);
                    if(!empty($this->input->post('img')))
                    {
                        $this->sub_img($editId);
                    }

                    $valid['status'] = 200;
                    $valid['msg']    = "Done ! Department Successfully Updated";
                }else{
                   $valid['msg']    = "This url already taken Please enter valid one"; 
                }

            }else
            {
                 if(!empty($file_data['file_name']))
                {
                    $imagePath       = $file_data['file_name'];
                    $data['path']    = $imagePath;
                }
                $data['created_at']  = date('Y-m-d,H:i:s');
                $dup = $this->common->checkDuplicateData('departments',array('slug' => $this->input->post('slug')));
            
                if($dup){
                    $data['slug'] = $this->input->post('slug');
                    $insert       = $this->common->insertData('departments',$data);
                    //$this->common->insertData('blog',$data);
                    if($insert)
                    {   
                        if(!empty($this->input->post('img')))
                        { 
                            $this->sub_img($insert);
                        }
                        $valid['status'] = 200;
                        $valid['msg']    = "Good Job ! Department Successfully Added";
                    }

                    //$valid['msg']  = 'Request Successfully Completed';
                    //$valid['status'] = 200;
                }else {
                    $valid['msg']  = 'This slug already Taken. Please try much better one';
                }

                
            }
        }else
        {
          $valid['msg'] = "Please Fillout all required Fields !";
        }
      
        echo json_encode($valid);
    }
    function sub_img($id)
    {
        define('PATH_IMG', './uploads/innerdepartment/');
        create_folder('uploads/innerdepartment','index','index');
         $data_count = count($this->input->post('img'));
        //if($data_count !=0){
        $image = array();
        for($i = 0; $i < $data_count; $i++)
        {
            $img  = $this->input->post('img')[$i];
            $pos  = strpos($img, ';');
    
            $name = microtime().'.'.explode('/', substr($img, 0, $pos))[1];
            $name = str_replace(' ', '_', $name);
            $img  = str_replace('data:image/png;base64,', '', $img);
            $img  = str_replace('data:image/jpeg;base64,', '', $img);
            $img  = str_replace('data:image/jpg;base64,', '', $img);
            $img  = str_replace(' ', '+', $img);
            $data = base64_decode($img,true);
            if(file_put_contents(PATH_IMG.$name, $data))
            {
                 $uploadImgData[$i]['images']        = $name;
                 $uploadImgData[$i]['department_id'] = $id;
                 $uploadImgData[$i]['created_at']    = date('Y-m-d,H:i:s');
                 $uploadImgData[$i]['status']        = 1;
            }
        }
        if(!empty($uploadImgData))
        {
            $this->common->multiple_insert('department_images',$uploadImgData);
        }
        return true;
    }

    function delete()
    {    
        $valid['success'] = array('status'=>400,'msg'=>array());
        $id      = decryptor($this->input->post('id'));
        if(!empty($id)) {
            $dep = $this->common->get_row('departments','*',array('id'=>$id));
            $this->common->deleteData('departments',array('id'=>$id),'department',$dep->path);
            if(!empty($dep)){
                
            $depmages = $this->common->get_alldata('*','department_images',array('department_id'=>$id));
           
            if(count($depmages) > 0){
             foreach($depmages as $ofImg){
                if(file_exists('uploads/innerdepartment/'.$ofImg->images))
                {
                    unlink('uploads/innerdepartment/'.$ofImg->images);
                }
             }
             $this->common->deleteData('department_images',array('department_id'=>$id),'','');
            }

            }
            $valid['msg']    = 'this Item was deleted';
            $valid['status'] = 200;
        }
        echo json_encode($valid);
    }


}


// SELECT d.name,w.doctors
//   FROM departments w
//   left JOIN doctors d ON JSON_CONTAINS(w.doctors, CONCAT('\"', d.id, '\"'))