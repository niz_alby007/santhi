<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Headers extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->model('category_model');
		$this->load->library('form_validation');
		
   }

	public function index()
	{	
		$pageTitle = "Headers";
		$district = $this->common->get_alldata('*','categories',array('status' => 1),'id ASC');
		$this->addData(compact('pageTitle','district'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/category_list'
                ]
            ]);
		$this->render("categories");
	}
	public function list(){
		$data 	  = array();
		$postData = $this->input->post();
		$data     = $this->category_model->categoryList($postData);
     	echo json_encode($data);

	}
	public function create($id=false) {
		if(!empty($id)){
			$id = decryptor($id);
			$pageTitle = "Edit categories";
			$editdata  = $this->common->get_row('categories','*',array('id'=>$id));
		}else{
			$editdata  = '';
			$pageTitle = "Create categories";
		}
		$this->addData(compact('pageTitle','editdata'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/category'
                ]
            ]);
		$this->render("create_category");
	}
	public function submit()
	{
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('slug','Municipality','required');
		$this->form_validation->set_rules('category','District','required');
		$id = $this->input->post('id');
		if($this->form_validation->run() == TRUE) {

			$data = array(
				'category' => $this->input->post('category'),
				'status'   => 2,
				'created_by' => $this->user_id,
			);
			if(!empty($id)) {
				$id = decryptor($id);
				$this->common->update_data('categories',$data,array('id' => $id));
				$valid['msg']  = 'Edit Successfully Completed';
				$valid['status'] = 200;
			}else{
				$data['slug']       = $this->input->post('slug');
				$data['created_at'] = date('Y-m-d,H:i:s');
				$dup = $this->common->checkDuplicateData('categories',array('slug'=>$this->input->post('slug')));
				if($dup){
					$this->common->insertData('categories',$data);
					$valid['msg']  = 'Request Successfully Completed';
					$valid['status'] = 200;
				}else {
					$valid['msg']  = 'This slug already Taken. Please try much better one';
				}
				
			}
		}else{

		}
		echo json_encode($valid);
	}
	function checkDuplicate() {
		$slug = $this->input->post('slug');
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		if(!empty($slug)){
			$return = $this->common->checkDuplicateData('departments',array('slug'=>$slug));
			if($return){
				$valid['status'] = 200;
				$valid['msg'] = 'success';
			}else{
				$valid['msg'] = "Please try much better one";
			}
		}
		echo json_encode($valid);
	}

	function checkDuplicate_fici() {
		$slug = $this->input->post('slug');
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		if(!empty($slug)){
			$return = $this->common->checkDuplicateData('facilities',array('slug'=>$slug));
			if($return){
				$valid['status'] = 200;
				$valid['msg'] = 'success';
			}else{
				$valid['msg'] = "Please try much better one";
			}
		}
		echo json_encode($valid);
	}
	
}