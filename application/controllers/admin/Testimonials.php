<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends ACP_Controller {

function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
          $this->load->model('common');
          $this->load->model('testimonial_model');
          $this->load->library('form_validation');
          $this->load->model('Image_uploading_model');
          
		
   }

   function index() {
    $pageTitle = "Testimonials List";
    $this->addData(compact('pageTitle'));
    $this->addAssets([
            'footer' => [
                //'admin/script/datatable',
                'admin/script/testimonial'
            ]
        ]);
    $this->render("testimonial/index");
   }

   public function list(){
        $data 	  = array();
        $postData = $this->input->post();
        $data     = $this->testimonial_model->List($postData);
        echo json_encode($data);

    }

    public function create($id=false) {
        if(!empty($id)){
            $id = decryptor($id);
            $pageTitle = "Edit Testimonial";
            $editdata  = $this->common->get_row('testimonial','*',array('id'=>$id));
        }else{
            $editdata  = '';
            $pageTitle = "Add Testimonial";
        }
        $this->addData(compact('pageTitle','editdata'));
        $this->addAssets([
                'footer' => [
                    //'admin/script/datatable',
                    'admin/script/testimonial'
                ]
            ]);
        $this->render("testimonial/create");
    }


    public function submit()
	{ 
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('name','Name','required');
		$id      = $this->input->post('id');
		$oldPath = $this->input->post('path');
		if($this->form_validation->run() == TRUE) {
			$file_data = $this->Image_uploading_model->image_uploader('','testimonial');

			$data = array(
				'name'         => $this->input->post('name'),
				'designation'  => $this->input->post('designation'),
				'description'  => $this->input->post('description'),
				'status'       => 1,
			);
			if(!empty($id)) {
                $imgvalid     = img_update($file_data,'testimonial',$oldPath);
				if(!empty($imgvalid)){
					$data['path'] = $imgvalid;
				}
				 
				$id    = decryptor($id);
				
				$data['updated_at'] = date('Y-m-d,H:i:s');
				$data['updated_by'] = $this->user_id;
				$this->common->update_data('testimonial',$data,array('id' => $id));
				$valid['msg']  = 'Slider Successfully Updated';
				$valid['status'] = 200;
			}else{
				if(!empty($file_data['file_name']))
				{
					$imagePath          = $file_data['file_name'];
					$data['path']       = $imagePath;
				}
				$data['created_at'] = date('Y-m-d,H:i:s');
				$data['created_by'] = $this->user_id;
				$this->common->insertData('testimonial',$data);
				$valid['msg']       = 'Slider Successfully Created';
				$valid['status']    = 200;
			}
		}else{
			$valid['msg']  = 'Please fillout all required fields';
		}
		echo json_encode($valid);
	}


}