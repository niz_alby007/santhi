<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->library('form_validation');
		$this->load->model('Image_uploading_model');
		
   }

	public function index()
	{	
		$pageTitle = "About US ";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_about'
                ]
            ]);
		$this->render("main_about_us");
	}
	public function homedata()
	{	
		$pageTitle = "Home Page Datas ";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/ft_about'
                ]
            ]);
		$this->render("home_about_us");
	}



	public function main_submit()
	{
		$addData = $this->input->post();
		foreach ($addData as $name => $value) 
		{
			update_app($name,$value);
		}
		
		if(isset($_FILES['about_chairman_file']['name'])){
			$chairman_file_oldPath   = $this->input->post('chairman_oldpath'); 
			$about_chairman_file = $this->imageuploader('about_chairman_file','aboutus','aboutus',$chairman_file_oldPath);
		  if ($about_chairman_file) {
			update_app('about_chairman_file',$about_chairman_file);
			
		  }
		}

		if(isset($_FILES['about_banner']['name'])){
			$about_file_oldPath   = $this->input->post('about_file_oldpath'); 
			$about_file = $this->imageuploader('about_banner','aboutus','aboutus',$about_file_oldPath);
		  if ($about_file) {
			update_app('about_banner',$about_file);
			
		  }
		}
	  
	  
		if(isset($_FILES['about_vission_file']['name'])){
			$oldPath   = $this->input->post('vssion_oldpath'); 
			$about_vission_file = $this->imageuploader('about_vission_file','aboutus','aboutus',$oldPath);
		  if ($about_vission_file) {
			update_app('about_vission_file',$about_vission_file);
			
		  }
		}
	  
		if(isset($_FILES['about_mission_file']['name'])){
			$oldPath   = $this->input->post('mission_oldpath'); 
			$about_vission_file = $this->imageuploader('about_mission_file','aboutus','aboutus',$oldPath);
		  if ($about_vission_file) {
			update_app('about_mission_file',$about_vission_file);
			
		  }
		}
	  
		if(isset($_FILES['about_greeting_file']['name'])){
			$oldPath   = $this->input->post('greeting_oldpath'); 
			$about_vission_file = $this->imageuploader('about_greeting_file','aboutus','aboutus',$oldPath);
		  if ($about_vission_file) {
			update_app('about_greeting_file',$about_vission_file);
			
		  }
		}
		$this->session->set_flashdata('msg','Success...!');
		redirect('admin/about_us');

	}


	function imageuploader($file,$folder,$path,$oldpath){
		if(!empty($_FILES[$file]['name'])){
			$file_data = $this->Image_uploading_model->fun_image_uploader($file,$folder,$path);
			$imagePath = $file_data['file_name'];
  
			  if(file_exists('./uploads/'.$folder.'/'.$oldpath)){
				if(!empty( $oldpath)){ unlink('./uploads/'.$folder.'/'.$oldpath); }
				  }else{
					//echo "Updation File not on location"; 
					  $this->session->set_flashdata('fmsg','Updation File not on location');
				  }
			return  $imagePath;
		  }else{
			return false;
	  }
   }

}