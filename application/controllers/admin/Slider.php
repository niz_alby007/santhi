<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends ACP_Controller {

function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
          $this->load->model('common');
          $this->load->model('slider_model');
          $this->load->library('form_validation');
          $this->load->model('Image_uploading_model');
          
		
   }

   function index() {
    $pageTitle = "Slider List";
    $this->addData(compact('pageTitle'));
    $this->addAssets([
            'footer' => [
                //'admin/script/datatable',
                'admin/script/slider'
            ]
        ]);
    $this->render("slider/index");
   }

   public function list(){
        $data 	  = array();
        $postData = $this->input->post();
        $data     = $this->slider_model->List($postData);
        echo json_encode($data);

    }

    public function create($id=false) {
        if(!empty($id)){
            $id = decryptor($id);
            $pageTitle = "Edit Slider";
            $editdata  = $this->common->get_row('slider','*',array('id'=>$id));
        }else{
            $editdata  = '';
            $pageTitle = "Add Slider";
        }
        $this->addData(compact('pageTitle','editdata'));
        $this->addAssets([
                'footer' => [
                    //'admin/script/datatable',
                    'admin/script/slider'
                ]
            ]);
        $this->render("slider/create");
    }


    public function submit()
	{ 
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('title','Title','required');
		$id      = $this->input->post('id');
		$oldPath = $this->input->post('path');
		if($this->form_validation->run() == TRUE) {
			$file_data = $this->Image_uploading_model->image_uploader('','slider');

			$data = array(
				'title'        => $this->input->post('title'),
				'description'  => $this->input->post('description'),
				'status'       => 1,
			);
			if(!empty($id)) {
                $imgvalid     = img_update($file_data,'slider',$oldPath);
				if(!empty($imgvalid)){
					$data['path'] = $imgvalid;
				}
				 
				$id    = decryptor($id);
				
				$data['updated_at'] = date('Y-m-d,H:i:s');
				$data['updated_by'] = $this->user_id;
				$this->common->update_data('slider',$data,array('id' => $id));
				$valid['msg']  = 'Slider Successfully Updated';
				$valid['status'] = 200;
			}else{
				if(!empty($file_data['file_name']))
				{
					$imagePath          = $file_data['file_name'];
					$data['path']       = $imagePath;
				}
				$data['created_at'] = date('Y-m-d,H:i:s');
				$data['created_by'] = $this->user_id;
				$this->common->insertData('slider',$data);
				$valid['msg']       = 'Slider Successfully Created';
				$valid['status']    = 200;
			}
		}else{
			$valid['msg']  = 'Please fillout all required fields';
		}
		echo json_encode($valid);
	}


}