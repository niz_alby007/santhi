<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends ACP_Controller {

function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		
   }

	public function index()
	{	$pageTitle = "Home";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    //'frond/script/home_link'
                ]
            ]);
		$this->render("home");
	}
	public function Restricted() {
		$pageTitle = "Restricted";
		$msg = "you cannot access this page because it is restricted";
		$this->addData(compact('pageTitle','msg'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    //'frond/script/home_link'
                ]
            ]);
		$this->render("restricted");
	}
}
