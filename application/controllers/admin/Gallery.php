<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends ACP_Controller {

function __construct() {
		parent::__construct();
        $this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('common');
		$this->load->model('gallery_model');
		$this->load->library('form_validation');
		$this->load->model('Image_uploading_model');
		
   }

	public function index()
	{	
		$pageTitle = "Services";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/gallery'
                ]
            ]);
		$this->render("gallery/index");
	}
	public function list(){
		$data 	  = array();
		$postData = $this->input->post();
		$data     = $this->gallery_model->breakingList($postData);
     	echo json_encode($data);

	}
	public function create($id=false) {
		if(!empty($id)){
			$id = decryptor($id);
			$pageTitle = "Edit Service";
			$editdata  = $this->common->get_row('gallery','*',array('id'=>$id));
		}else{
			$editdata  = '';
			$pageTitle = "Create Service";
		}
		$this->addData(compact('pageTitle','editdata'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/gallery'
                ]
            ]);
		$this->render("gallery/create");
	}
	public function submit()
	{
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('title','Title','required');
		$id = $this->input->post('id');
		$oldPath = $this->input->post('path');
		if($this->form_validation->run() == TRUE) {
			$file_data = $this->Image_uploading_model->image_uploader('','gallery');

			$data = array(
				'title'        => $this->input->post('title'),
				'status'       => 1,
			);
			if(!empty($id)) {
				if(!empty($file_data['file_name']))
				{
                	$imagePath    = $file_data['file_name'];
					$data['path'] = $imagePath;
            
					if(!empty($oldPath))
					{
						if(file_exists('./uploads/gallery/'.$oldPath))
						{
							if(unlink('./uploads/gallery/'.$oldPath))
							{

							}
						}
					}
				}
				 
				$id    = decryptor($id);
				$data['updated_at'] = date('Y-m-d,H:i:s');
				$data['updated_by'] = $this->user_id;
				$this->common->update_data('gallery',$data,array('id' => $id));
				$valid['msg']  = 'News Successfully Updated';
				$valid['status'] = 200;
			}else{
				if(!empty($file_data['file_name']))
				{
					$imagePath          = $file_data['file_name'];
					$data['path']       = $imagePath;
				}
				$data['created_at'] = date('Y-m-d,H:i:s');
				$data['created_by'] = $this->user_id;
				$this->common->insertData('gallery',$data);
				$valid['msg']       = 'Service Successfully Created';
				$valid['status']    = 200;
			}
		}else{
			$valid['msg']  = 'Please fillout all required fields';
		}
		echo json_encode($valid);
	}

	function checkDuplicate() {
		$slug = $this->input->post('slug');
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		if(!empty($slug)){
			$return = $this->common->checkDuplicateData('services',array('slug'=>$slug));
			if($return){
				$valid['status'] = 200;
				$valid['msg'] = 'success';
			}else{
				$valid['msg'] = "Please try much better one";
			}
		}
		echo json_encode($valid);
	}

	function moreinfo($id =false)
	{
		$id = decryptor($id);
		if($id)
		{   
			
			$mainServiceId = encryptor($id);
			$editdata      = $this->common->get_row('services_info','*',array('service_id'=>$id));
			$service       = $this->common->get_row('services','*',array('id'=>$id));

			if(!empty($service))
			{
				$mainserviceTitle = $service->title;
			}else
			{
				$mainserviceTitle ='';
			}
			
			if(!empty($editdata))
			{	$points        = $this->common->get_alldata('*','service_points',array('service_id'=>$editdata->id , 'status'=>1));	
				$updateId  = encryptor($editdata->id);
			}else
			{
				$updateId  ='';
				$points    = '';
			}
			$pageTitle     = "Edit Service Info : ".$mainserviceTitle;
		}else{
			$mainServiceId = '';
			$editdata  = '';
			$pageTitle = "Create Service Info";
			$points    = '';	
			$updateId  = '';
			$service   = '';
		}
		$this->addData(compact('pageTitle','editdata','points','updateId','mainServiceId','service'));
		$this->addAssets([
                'footer' => [
                	//'admin/script/datatable',
                    'admin/script/services'
                ]
            ]);
		$this->render("create_service_info");
	}

	public function info_submit()
	{
		$valid['success'] = array('status'=> 400 ,'msg'=>array());
		$this->form_validation->set_rules('title','Title','required');
		$id           = $this->input->post('srv_info_id');
		$main_service = $this->input->post('id');
		
		$oldPath = $this->input->post('path');
		if($this->form_validation->run() == TRUE) {
			$file_data = $this->Image_uploading_model->image_uploader('','services');

			$data = array(
				'title'        => $this->input->post('title'),
				'description'  => $this->input->post('description'),
				'meta_key'     => $this->input->post('meta_key'),
				'meta_description' => $this->input->post('meta_description'),
				'status'       => 1,
			);
			if(!empty($id)) {
				if(!empty($file_data['file_name']))
				{
                	$imagePath    = $file_data['file_name'];
					$data['path'] = $imagePath;
            
					if(!empty($oldPath))
					{
						if(file_exists('./uploads/services/'.$oldPath))
						{
							if(unlink('./uploads/services/'.$oldPath))
							{

							}
						}
					}
				}
				 
				$id    = decryptor($id);
				$up_id = $this->common->deleteDatafun('service_points',array('service_id' => $id));
				$data['updated_at'] = date('Y-m-d,H:i:s');
				$data['updated_by'] = $this->user_id;
				$this->common->update_data('services_info',$data,array('id' => $id));
				$point_filt = array_filter($this->input->post('servicepoint')); 
				$cpt = count($point_filt);
					for($i=0; $i<$cpt; $i++)
					{   
						$serpints[$i]['point']       =   $this->input->post('servicepoint')[$i];//$imageData['file_name'];
						$serpints[$i]['service_id '] = $id;
						$serpints[$i]['created_at']  = date('Y-m-d,H:i:s');
						$serpints[$i]['status']      = 1;
					}
					$this->common->multiple_insert('service_points',$serpints);
					
				$valid['msg']  = 'News Successfully Updated';
				$valid['status'] = 200;
			}else{
				if(!empty($file_data['file_name']))
				{
					$imagePath          = $file_data['file_name'];
					$data['path']       = $imagePath;
				}
				$data['service_id'] = decryptor($main_service);
				$data['created_at'] = date('Y-m-d,H:i:s');
				$data['created_by'] = $this->user_id;

				$restun = $this->common->insertData('services_info',$data);
				if($restun) 
				{
					$point_filt = array_filter($this->input->post('servicepoint')); 
					$cpt        = count($point_filt);
					for($i=0; $i<$cpt; $i++)
					{   
						$serpints[$i]['point']       =   $this->input->post('servicepoint')[$i];//$imageData['file_name'];
						$serpints[$i]['service_id '] = $restun;
						$serpints[$i]['created_at']  = date('Y-m-d,H:i:s');
						$serpints[$i]['status']      = 1;
					}
					$this->common->multiple_insert('service_points',$serpints);
				}
				$valid['msg']  = 'Service Successfully Created';
				$valid['status'] = 200;
			}
		}else{
			$valid['msg']  = 'Please fillout all required fields';
		}
		echo json_encode($valid);
	}

	function banner_submit()
	{
		
		 if(isset($_FILES['banner_img']['name']))
		 {
            $about_file_oldPath   = $this->input->post('banner_img'); 
            $about_file = $this->imageuploader('banner_img','banner_img','banner_img',$about_file_oldPath);
        	if ($about_file) 
			{
              update_app('banner_img',$about_file);
        	}
			$this->session->set_flashdata('msg','Success...!');
			redirect('admin/services/create');
		}
	}
	function imageuploader($file,$folder,$path,$oldpath){
		if(!empty($_FILES[$file]['name'])){
			$file_data = $this->Image_uploading_model->fun_image_uploader($file,$folder,$path);
			$imagePath = $file_data['file_name'];
  
			  if(file_exists('./uploads/'.$folder.'/'.$oldpath)){
				if(!empty( $oldpath)){ unlink('./uploads/'.$folder.'/'.$oldpath); }
				  }else{
					echo "Updation File not on location"; 
					  $this->session->set_flashdata('fmsg','Updation File not on location');
				  }
			return  $imagePath;
		  }else{
			return false;
	  }
   }

   function delete()
   {	
   		$valid['success'] = array('status'=>400,'msg'=>array());
   		$id      = decryptor($this->input->post('id'));
   		$path    = $this->input->post('path');
   		$folder  = $this->input->post('folder');
   		$table   = $this->input->post('tbl');
   		$tablId  = $this->input->post('tblid');
   		if(!empty($id)) {
			$serv = $this->common->get_row('services_info','*',array('service_id'=>$id));
   			$this->common->deleteData($table,array($tablId=>$id),'services',$path);
			if(!empty($serv)){
				$this->common->deleteData('services_info',array('service_id'=>$id),'services',$serv->path);
				$this->common->deleteData('service_points',array('service_id'=>$serv->id),'','');
			}
   			$valid['msg']    = 'this Item was deleted';
   			$valid['status'] = 200;
   		}
   		echo json_encode($valid);
   }
		
}