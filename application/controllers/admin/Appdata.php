<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appdata extends ACP_Controller {

function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('Image_uploading_model');
		
   }

	public function index()
	{	
		$pageTitle = "Appdata";
		$this->addData(compact('pageTitle'));
		$this->addAssets([
                'footer' => [
                	'admin/script/appdata',
                    //'frond/script/home_link'
                ]
            ]);
		$this->render("appdata");
	}
	function submit(){
		$data = $this->input->post();
		foreach ($data as $name => $value) {
			 update_app($name,$value);
		}
		 if(isset($_FILES['festival_img']['name'])){
              $about_file_oldPath   = $this->input->post('festival_img'); 
              $about_file = $this->imageuploader('festival_img','festival_img','festival_img',$about_file_oldPath);
        if ($about_file) {
              update_app('festival_img',$about_file);
              
        }
    }

		$this->session->set_flashdata('msg','Success...!');
		redirect('admin/appdata');
	}

		function home_submit(){
		$data = $this->input->post();
		foreach ($data as $name => $value) {
			 update_app($name,$value);
		}
		 
		$this->session->set_flashdata('msg','Success...!');
		redirect('admin/home-page-controler');
	}


	function imageuploader($file,$folder,$path,$oldpath){
	  if(!empty($_FILES[$file]['name'])){
	      $file_data = $this->Image_uploading_model->fun_image_uploader($file,$folder,$path);
	      $imagePath = $file_data['file_name'];

	        if(file_exists('./uploads/'.$folder.'/'.$oldpath)){
	          if(!empty( $oldpath)){ unlink('./uploads/'.$folder.'/'.$oldpath); }
	            }else{
	              echo "Updation File not on location"; 
	                $this->session->set_flashdata('fmsg','Updation File not on location');
	            }
	      return  $imagePath;
	    }else{
	      return false;
	}
 }

}
