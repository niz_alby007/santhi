<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends Page_Controller {
	function __construct() {
		  parent::__construct();
          $this->load->helper('url');
		  $this->load->helper('date');
		  $this->load->model('common');
		  $this->load->model('frond_model');

   }
	
	public function index()
	{
		$pageTitle= "Gallery";
		$gallery = $this->common->get_alldata('*','gallery',array('status' =>1 ));
		$this->addData(compact('pageTitle','gallery')); 
		$this->addAssets([
                'footer' => [
                	//'admin/page_link/datatable',
                    //'frond/page_link/home' 
                ]
            ]);
			$this->render("gallery"); 
	}
}