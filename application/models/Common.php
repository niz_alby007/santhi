<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Model {

	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 }
	function get_alldata($select='*',$table=FALSE,$condition=FALSE,$order = FALSE,$limit = FALSE,$start = FALSE) {

		$this->db->select($select);
	  	$this->db->from($table);
	  	$this->db->where($condition);
      	if (!empty($limit)) {
          $this->db->limit($limit, $start);
      	}
	    if($order){
	         $this->db->order_by($order);
	    }
	    return  $this->db->get()->result();
	}
	function get_department($limit=FALSE,$start = FALSE)
	{

			$this->db->select('*,c.category');
	  	$this->db->from('departments as d');
	    $this->db->join('categories as c','c.category_id =d.department_id','left');
		  $this->db->order_by("id", "ASC");
		   if (!empty($limit)) {
          $this->db->limit($limit, $start);
      	}
	    return  $this->db->get()->result();
	}

	function insertData($table,$data){
	    if($data){
	      $this->db->insert($table, $data);
	       return $idOfInsertedData = $this->db->insert_id();
	    }
  	}

	public function entry_update($table,$where,$set) 
	{
		$this->db->where($where);
		$this->db->update($table, $set);
		return true;
	}

  function update_counter($slug) {
	$number = rand(10,100);
  // return current article views 
      $this->db->where('slug', urldecode($slug));
      $this->db->select('visitors');
      $count = $this->db->get('news')->row();
  // then increase by one 
      $this->db->where('slug', urldecode($slug));
      $this->db->set('visitors', ($count->visitors + $number));
      $this->db->update('news');
  }

	public function get_count_inarray($condition,$where_in=FALSE,$table=FALSE,$subcate=false) {

	    $this->db->select('*');
	    $this->db->from($table);
	    $this->db->where($condition);
	    $this->db->where_in('status',$where_in);
    	return $this->db->count_all_results();

    }

  	function get_row($table,$select='*',$where=FALSE,$order = FALSE,$limit = FALSE,$start = FALSE){
    	$this->db->select($select);
      	$this->db->from($table);
      	$this->db->where($where);
		if (!empty($limit)) {
			$this->db->limit($limit, $start);
			}
		if($order){
			   $this->db->order_by($order);
		}
      	$query=$this->db->get();
      	$row=$query->row();
      	return $row;
  	}
  	public function update_data($table,$set,$where) {
	    $this->db->where($where);
	    $this->db->update($table, $set);
  	}
  	public function deleteData($table,$condition,$folder=FALSE,$path = FALSE){
    	if(!empty($path )){
    		
        	if(file_exists('uploads/'.$folder.'/'.$path)){
           		unlink('uploads/'.$folder.'/'.$path);
        	}
      	}
      	$this ->db->where($condition);
      	$this ->db->delete($table);
  	}
	public function deleteDatafun($table,$where)
	{
		if($where)
		{
		  $this->db-> where($where);
		  $update = $this->db->delete($table);
		  return $update;
		}
	}

  	function checkDuplicateData($table=FALSE,$condition = FALSE) {
  		//$this->db->select($select);
  		$this->db->where($condition);
    	$query = $this->db->get($table);
    	$count_row = $query->num_rows();
    	if($count_row > 0) {
    		return FALSE;
    	}else{
    		return TRUE;
    	}
  	}
	public function multiple_insert($table = false,$image = array())
	{
		return $this->db->insert_batch($table,$image);
	}

	function count_row($select='*',$table=FALSE,$condition = FALSE)
	{
		$this->db->select($select);
  		$this->db->where($condition);
    	$query = $this->db->get($table);
    	$count_row = $query->num_rows();
    	return $count_row;
	}

}

