<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frond_model extends CI_Model {

	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 }
	function get_alldatas($select='*',$table=FALSE,$condition=FALSE,$in=false,$order = FALSE,$limit = FALSE,$start = FALSE) {
		$this->db->select($select);
	  	$this->db->from($table);
	  	  	if(!empty($condition)){
	   			$this->db->where($condition);
	   			//$this->db->where("FIND_IN_SET(5,main_category) !=", 0);
	   		}
		   	if(!empty($in)){
		   		$this->db->where("FIND_IN_SET($in,main_category) !=", 0);
		   	}
      		if ($limit != '' && $start != '') {
          		$this->db->limit($limit, $start);
      		}
		    if($order){
		         $this->db->order_by($order);
		    }
	    return  $this->db->get()->result();
	}
	function findin_set($select='*',$table=FALSE,$status=FALSE,$in=false,$order = FALSE,$limit = FALSE,$start = false){
		$result = $this->db->query("SELECT * FROM $table WHERE status = $status AND FIND_IN_SET(".$in.",main_category) ORDER BY news_id desc LIMIT $limit ")->result();
		return $result;
	}
}