<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department_model extends CI_Model {

	function __construct(){
	    parent::__construct();
	    $this->load->database();
	}

    public function get_product_list_developer_data($table,$search=FALSE,$category=FALSE,$page=FALSE,$per_page=FALSE)
    {
      
        $this->db->select('st.state_name,dis.district_name,c.city_name,t1.*,d.developer_name,d.developer_logo');
        $this->db->from('properties as t1');
        
       $this->db->join('property_location as p', 'p.property_parent_id = t1.properties_id', 'left');
       $this->db->join('developers as d', 'd.developer_id = t1.developer_id', 'left');
       $this->db->join('city as c', 'c.city_id = p.property_city', 'left');
       $this->db->join('district as dis', 'dis.district_id = c.district_id', 'left');
       $this->db->join('state as st', 'st.state_id = c.state_id', 'left');
       if(!empty($category))
       {
        $this->db->where('t1.category ',$category);
       }
       $this->db->where('t1.developer_id !=',0);
       $this->db->where('t1.status !=',5);
    
        //$this->db->order_by('t1.properties_id','desc');
        $this->db->order_by('t1.updated_at','desc');
    
         if(!empty($searchkey)){
             
             $key = explode(' ', $searchkey);
              
             foreach ($key as $keyword) {
            
             if($keyword == "rent" || $keyword == "Rent"){
                 $this->db->where('t1.category', 'Rent');
             }
             if($keyword == "sale"){
                 $this->db->where('t1.category', 'Buy');
             }
             if($keyword == "villa" || $keyword == "house" || $keyword =="flat" || $keyword =="appartment" || $keyword =="pg" || $keyword =="guesthouse" || $keyword =="warehouse" || $keyword =="shop" || $keyword =="land" || $keyword =="office"  ){
                  $this->db->where('t1.property_type', $keyword);
             }else{
                 // $this->db->like('t1.profile_id', $keyword);
             }
          }
        }
        if($search){
            // $key = explode(' ', $search);
            // foreach ($key as $keyword) {
            //   $this->db->like('t1.keywords', $keyword);
            //    $this->db->or_like('t1.keywords', $keyword);
            // }
             $this->db->where('d.developer_name ',$search);
          }
        if($per_page){
        $this->db->limit($per_page, $page);
        }
        $this->db->group_by('t1.developer_id');
        
          return $this->db->get()->result();
    } 

    public function get_product_list_developer_data_no_rows($table,$search=FALSE){

        $this->db->select('*');
        $this->db->from($table);
        //$this->db->where($table.'status',1);
        $this->db->join('developers as d', 'd.developer_id = properties.properties_id', 'left');
        $this->db->where('properties.status',1);
        $this->db->where('properties.developer_id !=',0);
    
        if($search){
        
           $this->db->where('d.developer_name ',$search);
        
        }
        return $this->db->count_all_results();
      }
  function get_department_finder($cond=FALSE)
  {
    $this->db->select('t1.*,c.*');
    $this->db->from('departments as t1');
    $this->db->join('categories as c', 'c.category_id = t1.department_id', 'left');
    $this->db->where($cond);
    return $this->db->get()->row();
  }
    
}