<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author_model extends CI_Model {

	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 }

	function authorList($postData = False) {
	      $response = array();
	     ## Read value
	      $draw = $postData['draw'];
	      $start = $postData['start'];
	      $rowperpage = $postData['length']; // Rows display per page
	      $columnIndex = $postData['order'][0]['column']; // Column index
	      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
	      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
	      $searchValue = $postData['search']['value']; // Search value

	      ## Search 
	      $searchQuery = "";
	      if($searchValue != ''){
	          $searchQuery = " (name like '%".$searchValue."%' or user_name like '%".$searchValue."%') ";
	      }


	      ## Total number of records without filtering
	      $this->db->select('count(*) as allcount');
	      $this->db->where('login_status !=',1);
	      $records = $this->db->get('login')->result();
	      $totalRecords = $records[0]->allcount;

	      ## Total number of record with filtering
	      $this->db->select('count(*) as allcount');
	      $this->db->where('login_status !=',1);
	      if($searchQuery != '')
	      $this->db->where($searchQuery);
	      $records = $this->db->get('login')->result();
	      $totalRecordwithFilter = $records[0]->allcount;

	      
	      ## Fetch records
	      $this->db->select('*');
	      $this->db->where('login_status !=',1);
	      if($searchQuery != '')
	      $this->db->where($searchQuery);
	      $this->db->order_by($columnName, $columnSortOrder);
	      $this->db->limit($rowperpage, $start);
	      $records = $this->db->get('login')->result();

	      $data = array();
	      $i=1;
	      foreach($records as $record ){
  			
  			if($record->login_status ==1 ) {
  				$status = 'Developer';
  			}else if ($record->login_status == 2) {
  				$status = 'Admin';
  			}elseif ($record->login_status == 3) {
  				$status = 'Staff';
  			}else{
  				$status = "Author";
  			}

			$profile ="<img src='".profile_dp('profile',$record->profile)."' width='60px' />";
         
          $data[] = array( 
            //'no'=>$i,
             "login_id"  => $record->login_id,
             "name"      => $record->name,
			 "broker_id" => $record->broker_id,
			 "profile"   => $profile,
             "user_name" => $record->user_name,
             'status'   => $status,
              "login_status"=>'<div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.base_url('admin/authors/create/').encryptor($record->login_id).'"><i class="fa fa-pen"></i>Edit</a>
                             <a class="dropdown-item" href="javascript:void(0)" onclick="deletecategory(this)" data-id="'.encryptor($record->login_id).'" data-tbl="login" data-folder="" data-path="" data-tblid="login_id"><i class="fa fa-trash"></i>Delete</a>
                          </div>
                        </div>',
              
          ); 
	       $i++; }
	      ## Response
	      $response = array(
	          "draw" => intval($draw),
	          "iTotalRecords" => $totalRecords,
	          "iTotalDisplayRecords" => $totalRecordwithFilter,
	          "aaData" => $data
	      );
	      return $response; 
	}
}
