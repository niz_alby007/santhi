<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctors_model extends CI_Model {

	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 }

	function doctorsList($postData = False) {
	      $response = array();
	     ## Read value
	      $draw = $postData['draw'];
	      $start = $postData['start'];
	      $rowperpage = $postData['length']; // Rows display per page
	      $columnIndex = $postData['order'][0]['column']; // Column index
	      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
	      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
	      $searchValue = $postData['search']['value']; // Search value

	      ## Search 
	      $searchQuery = "";
	      if($searchValue != ''){
	          $searchQuery = " (name like '%".$searchValue."%' or  degree_for_surgeons like '%".$searchValue."%' or contact_number like '%".$searchValue."%') ";
	      }


	      ## Total number of records without filtering
	      $this->db->select('count(*) as allcount');
	      $records = $this->db->get('doctors')->result();
	      $totalRecords = $records[0]->allcount;

	      ## Total number of record with filtering
	      $this->db->select('count(*) as allcount');
	      if($searchQuery != '')
	      $this->db->where($searchQuery);
	      $records = $this->db->get('doctors')->result();
	      $totalRecordwithFilter = $records[0]->allcount;

	      
	      ## Fetch records
	      $this->db->select('*');
		  $this->db->order_by("id", "ASC");
	      if($searchQuery != '')
	      $this->db->where($searchQuery);
	      $this->db->order_by($columnName, $columnSortOrder);
	      $this->db->limit($rowperpage, $start);
	      $records = $this->db->get('doctors')->result();

	      $data = array();
	      $i=1;
	      foreach($records as $record ){
  			
            $img = "<img src='".img_vlid('doctors',$record->path)."' width='100px' />";
         
          $data[] = array( 
            //'no'=>$i,
             "id"          => $record->id,
             "name"        => $record->name,
             "degree_for_surgeons" => $record->degree_for_surgeons,
             "path"        => $img,
             "contact_number" => $record->contact_number,
             "status"      =>'<div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.base_url('admin/doctors/create/').encryptor($record->id).'"><i class="fa fa-pen"></i>Edit</a>
                             <a class="dropdown-item" href="javascript:void(0)" onclick="deleteDoctors(this)" data-id="'.encryptor($record->id).'" data-tbl="doctors" data-folder="doctors" data-path="'.$record->path.'" data-tblid="id"><i class="fa fa-trash"></i>Delete</a>
                          </div>
                        </div>',
              
          ); 
	       $i++; }
	      ## Response
	      $response = array(
	          "draw" => intval($draw),
	          "iTotalRecords" => $totalRecords,
	          "iTotalDisplayRecords" => $totalRecordwithFilter,
	          "aaData" => $data
	      );
	      return $response; 
	}

	function doctors($table = False, $select = '*',$where= FALSE, $wherein = FALSE,$order = FALSE)
	{ 
			$this->db->select($select);
	  	$this->db->from(''.$table .' as doc');
	  	//$this->db->join('departments as d','de.department_id = IN('.$wherein.')');
	  	
	  	if($where)
	  	{
	  		$this->db->where($where);
	  	}
	  	if(!empty($wherein))
	  	{
	  		$doctorsid  = implode(',',$wherein);
	  		$this->db->where_in('doc.id',$wherein);
	  	}
      if (!empty($limit)) {
          $this->db->limit($limit, $start);
      }
	    if($order){
	         $this->db->order_by($order);
	    }
	    return  $this->db->get()->result();
	}
}
