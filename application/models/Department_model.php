<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department_model extends CI_Model {

	function __construct(){
	    parent::__construct();
	    $this->load->database();
	 }

	function departmentList($postData = False) {
	      $response = array();
	     ## Read value
	      $draw = $postData['draw'];
	      $start = $postData['start'];
	      $rowperpage = $postData['length']; // Rows display per page
	      $columnIndex = $postData['order'][0]['column']; // Column index
	      $columnName = $postData['columns'][$columnIndex]['data']; // Column name
	      $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
	      $searchValue = $postData['search']['value']; // Search value

	      ## Search 
	      $searchQuery = "";
	      if($searchValue != ''){
	          $searchQuery = " (department_id like '%".$searchValue."%' or  doctors like '%".$searchValue."%' or short_description like '%".$searchValue."%') ";
	      }


	      ## Total number of records without filtering
	      $this->db->select('count(*) as allcount');
	      $records = $this->db->get('departments')->result();
	      $totalRecords = $records[0]->allcount;

	      ## Total number of record with filtering
	      $this->db->select('count(*) as allcount');
	      if($searchQuery != '')
	      $this->db->where($searchQuery);
	      $records = $this->db->get('departments')->result();
	      $totalRecordwithFilter = $records[0]->allcount;

	      
	      ## Fetch records
	      $this->db->select('*,c.category');
	      $this->db->from('departments as d');
	      $this->db->join('categories as c','c.category_id =d.department_id','left');
		  $this->db->order_by("id", "ASC");
	      if($searchQuery != '')
	      $this->db->where($searchQuery);
	      $this->db->order_by($columnName, $columnSortOrder);
	      $this->db->limit($rowperpage, $start);
	      $records = $this->db->get()->result();

	      $data = array();
	      $i=1;
	      foreach($records as $record ){
  			
            $img = "<img src='".img_vlid('department',$record->path)."' width='100px' />";
         	$doctor = doctorsList(unserialize($record->doctors),'name');
          $data[] = array( 
            //'no'=>$i,
             "id"  => $record->id,
             "department_id"  => $record->category,
             "doctors"        => rtrim($doctor,','),
             "path"        => $img,
             "status"      =>'<div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="'.base_url('admin/departments/create/').encryptor($record->id).'"><i class="fa fa-pen"></i>Edit</a>
                             <a class="dropdown-item" href="javascript:void(0)" onclick="deleteDepartment(this)" data-id="'.encryptor($record->id).'" ><i class="fa fa-trash"></i>Delete</a>
                          </div>
                        </div>',
              
          ); 
	       $i++; }
	      ## Response
	      $response = array(
	          "draw" => intval($draw),
	          "iTotalRecords" => $totalRecords,
	          "iTotalDisplayRecords" => $totalRecordwithFilter,
	          "aaData" => $data
	      );
	      return $response; 
	}
}
