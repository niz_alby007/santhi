<?php

function user_login_check() {
   $CI =& get_instance();
  if($CI->session->has_userdata('logged_in') && $CI->session->userdata('logged_in') ==1){
    return true;
  }else {
    redirect('admin/login');
  }
}
function permission($loginId,$created_by=false)
{
  $CI =& get_instance();
  $CI->load->database();
  if(!empty($loginId)) {
     $query=$CI->db->select("*")->from('login')->where('login_id ',$loginId)->get()->row();
     if($query->login_status == 1)
     {
      $permission = TRUE;
     }else if($query->login_status == 2)
     {
      $permission = TRUE;
     }else if($query->login_id == $created_by){
      $permission = TRUE;
     }else
     {
      $permission = FALSE;
     }
    
     return  $permission ;
  }
}

function doctorsList($dc=FALSE,$column=FALSE)
{
  $CI =& get_instance();
  $CI->load->database();
  $CI->load->model('common');
  $CI->db->select($column);
  $CI->db->from('doctors');
  $CI->db->where_in('id',$dc);
  $query = $CI->db->get()->result();
 // echo $CI->db->last_query();
  $doctors = '';
  foreach ($query as $key) {
  //   array_push($doctors, $key->name);
      $doctors .= $key->$column.',';
   }
  return  $doctors;


//   $this->db
//     ->select("td.Date, GROUP_CONCAT(ts.student_name)")
//     ->from("tbl_students AS ts")
//     ->join("tbl_attendance AS ta","find_in_set(ts.st_id,ta.attendance)","left",false)
//     ->get();
 }
function __category(){
  $CI =& get_instance();
  $CI->load->database();
  $CI->load->model('common');
  return $CI->common->get_alldata('*','categories',array('status'=>1));
}
function __developers()
{
  $CI =& get_instance();
  $CI->load->database();
  $query=$CI->db->select("developer_name")->from('developers')->where('status',1)->get()->result();
  $dev= [];
  foreach($query as $data) {
    array_push($dev, $data->developer_name); 

  }
  echo json_encode($dev);
}
function last_activity(){

  $CI =& get_instance();
  $CI->load->database();
  $user_id = $CI->session->userdata('login_id');
    //$time_since = now() - $CI->session->userdata('last_activity');
    $interval = 300;

    // Update database
    $updated = $CI->db
          ->set('last_activity', date('Y-m-d,H:i:s'))
          ->where('login_id', $user_id)
          ->update('login');

    // Log errors if you please
    $updated or log_message('error', 'Failed to update last activity.');
}

function update_app($name,$value){
  //print_r($value);
  $CI =& get_instance();
  $CI->load->database();

  
  if(is_array($value))
  {
  //  $data_type='array';
    $value=serialize($value);
  }
  elseif(is_object($value))
  {
  //  $data_type='object';
    $value=serialize($value);
  }

  $data=array(
    'app_name'  => $name,
    'app_value' => $value,
    'status'    => 1,
    //'option_type'=>$data_type,
  );
  //$CI->db->insert('app_data',$data);
  $query = $CI->db->select('*')->from('app_data')->where('app_name',$name)->get();

  //if option already exists then update else insert new
  if($query->num_rows() < 1) return $CI->db->insert('app_data',$data);
  else                       return $CI->db->update('app_data',$data,array('app_name'=>$name));
}
function get_appdata($name){
  $CI =& get_instance();
  $CI->load->database();
  $query=$CI->db->select("*")->from('app_data')->where('app_name',$name)->get();
  //echo $CI->db->last_query();

  if($query->num_rows() < 1)return false;
    $option = $query->row();
    if($option->status){$value = $option->app_value;}
    return $value;
}


function __doctors() {
  $CI =& get_instance();
  $CI->load->database();
  $CI->db->select('*');
  $CI->db->from('doctors');
  $CI->db->where('status ',1);
  $count = $CI->db->get()->num_rows();
  return $count;
}

function __tblcounter($table=false,$con=false) {
  $CI =& get_instance();
  $CI->load->database();
  $CI->db->select('*');
  $CI->db->from($table);
  $CI->db->where($con);
  $count = $CI->db->get()->num_rows();
  return $count;
}

function __categoryCounter($category = FALSE , $status = FALSE) {
  $CI =& get_instance();
  $CI->load->database();
  $CI->db->select('*');
  $CI->db->from('properties');
  if (!empty($category)) {
   $CI->db->where('category',$category);
  }
  if (!empty($status)) {
      $CI->db->where('status',$status);
  }
  $count = $CI->db->get()->num_rows();
  return $count;
}

 function slugify($text=FALSE,  $divider = '-')
{
  // replace non letter or digits by divider
  $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, $divider);

  // remove duplicate divider
  $text = preg_replace('~-+~', $divider, $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function encryptor($string) {
    $output = false;
    $encrypt_method = "AES-128-CBC";
    //pls set your unique hashing key
    $secret_key = 'nisam';
    $secret_iv = 'developerchooice';
    // hash
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    //do the encyption given text/string/number
    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);
    return $output;
}

function decryptor( $string) {
    $output = false;
    $encrypt_method = "AES-128-CBC";
    //pls set your unique hashing key
    $secret_key = 'nisam';
    $secret_iv = 'developerchooice';
    // hash
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    //decrypt the given text/string/number
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}
function ourServices() {
  $CI =& get_instance();
  $CI->load->database();
 
    $query=$CI->db->select("*")->from('services')->where('status ',1)->order_by('id','desc')->get()->result();
    return $query;
}



function district() {
  $CI =& get_instance();
  $CI->load->database();
  $query=$CI->db->select("*")->from('location')->where('status',1)->get()->result();
  return $query;
}



function create_folder($path=FALSE,$page_name=FALSE, $controller_name=FALSE){
  $CI =& get_instance();
  $path = $path;
  if (!is_dir($path)){
      mkdir($path,0777,TRUE);
  }
   $file = $path.$controller_name.'.php';
  if(!file_exists($file)){
    // Create Controller
   $controller = fopen($path.'/'.$controller_name.'.php', "a") or die("Unable to open file!");

   $controller_content ="<?php
   defined('BASEPATH') OR exit('No direct script access allowed');";

    fwrite($controller, "\n". $controller_content);
    fclose($controller);
  }else{
    $CI->session->set_flashdata('fmsg','"The directory or file cannot be created" message appears when copying files');
  }
}

function img_vlid($folder,$imgName){
  if(!empty($imgName)){
    if (file_exists('./uploads/'.$folder.'/'.$imgName)) {
      $img = base_url('uploads/').$folder.'/'.$imgName;
    }else{
      $img = base_url('uploads/default.png');
    }

  }else{
    $img = base_url('uploads/default.png');
  }
  return $img;
}

function profile_dp($folder,$imgName)
{
  if(!empty($imgName)){
    if (file_exists('./uploads/'.$folder.'/'.$imgName)) {
      $img = base_url('uploads/').$folder.'/'.$imgName;
    }else{
      $img = base_url('frond/img/user_avatar.webp');
    }

  }else{
    $img = base_url('frond/img/user_avatar.webp');
  }
  return $img;
}

function img_update($file=FALSE,$folder=FALSE,$oldpath=FALSE) 
{
  if(!empty($file['file_name']))
	{			
    if(!empty($oldpath))
    {
      if(file_exists('./uploads/'.$folder.'/'.$oldpath))
      {
        if(unlink('./uploads/'.$folder.'/'.$oldpath))
        {

        }
      }
    }
    return $file['file_name'];
  }
}

function id_generator($string)
{
  $CI =& get_instance();
  if(!empty($string))
  {
    $CI->load->helper('string');
	  return  strtoupper(mb_substr($string, 0, 2)).'-'.random_string('numeric',5);
  }
}
function loger($id = FALSE){
  $CI =& get_instance();
  $result = $CI->db->query("SELECT * FROM login WHERE login_id = $id ")->result();
  if(!empty($result)){
    $name = $result[0]->name;
  }else{
     $name = "Arpis";
  }
  return $name;
}



function timeAgo($datetime,$full=FALSE) {
     $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
function categoryFinder ($id = false) {
  $CI =& get_instance();
  $CI->load->database();
  if(!empty($id)) {
     $query=$CI->db->select("*")->from('categories')->where_in('id ',$id )->get()->row();
  }
  return $query->category;
}



function login_check_user(){
  $CI =& get_instance();
  if($CI->session->has_userdata('login') && $CI->session->userdata('login') ==1){
    return true;
  }else {
    return 0;
  }
}


if ( ! function_exists('count_visitor')) {
    function count_visitor()
    {
        $filecounter= require_once(APPPATH.'libraries/scraper_utilities.php');

        $kunjungan=file($filecounter);
        $kunjungan[0]++;
        $file=fopen($filecounter, 'w');
        fputs($file, $kunjungan[0]);
        fclose($file);
        return $kunjungan[0];
    }
}


function __total($tbl,$count){
  $CI =& get_instance();
  $CI->load->database();
  $CI->db->select('*');
  $CI->db->from($tbl);
  $CI->db->where('status',1);
  $count = $CI->db->get()->num_rows();
  return $count;

}




?>