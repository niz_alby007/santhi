<?php

function __procategory(){
    $CI =& get_instance();
    //$CI->load->database();

    $CI->db->select('*');
    $CI->db->from('categories');
    $CI->db->where('status',1);
    $query = $CI->db->get()->result();
    return $query;

}
function __developer()
{
    $CI =& get_instance();
    $CI->db->select('*');
    $CI->db->from('developers');
    $CI->db->where('status',1);
    $query = $CI->db->get()->result();
    return $query;
}
function __country()
{
    $CI =& get_instance();
    $CI->db->select('*');
    $CI->db->from('country');
    $CI->db->where('status',1);
    $query = $CI->db->get()->result();
    return $query;
}
function shortlistproperty($id = false){
    $CI =& get_instance();
    $CI->db->select('*');
    $CI->db->from('shortlist');
    $CI->db->where('status',1);
    $CI->db->where('product_id',intval($id));
    $CI->db->where('active',1);
    $CI->db->where('logg_id',intval($CI->session->userdata('login_id')));
   return $CI->db->count_all_results();
}